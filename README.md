# Real-time Volumetric Cloud Visualization of Meteorological Simulation Data [![CC BY 4.0][cc-by-shield]][cc-by]
[![Banner](Images/superfar_still_rainlightning.jpg)](https://youtu.be/eGZ6YtlEago)

## Master Thesis Abstract (21. Juni. 2021)
I present a concept for visualizing realistic volumetric clouds in real-time in the Unreal Engine using a scientific weather simulation as a basis.

From the simulation files, I extract and compress non-linearly spaced variables such as cloud density, rainfall, and wind speed.
Cloud density is used as the basis for ray-marched volumetric clouds in Unreal Engine. The clouds use wind speed as a flow map and rain density as a color map. I add Voronoi noise to the clouds and mask it with wind speed to add dynamic detail. I am also able to animate all time steps of the simulation in real-time. 

A user study shows that most of the added effects improve realism and my implementation is five out of six times more realistic than previous methods. The median performance of a playable prototype on consumer-grade hardware is 128~FPS and meets the performance target.

[Thesis Full Text](Cloud_Master_Thesis_MNM_21_06_21.pdf)


## Overview
This git repository contains the source files of my master thesis. Everything I tried can be found here, as well as the final prototypes. Note that the generated textures are not in the git repository, they are separate on [Open Data LMU](https://doi.org/10.5282/ubm/data.311).
- [Updates Post Submission](#updates-post-submission) is an overview of what has changed since the master thesis was created. 
- [Setup](#setup) explains how to get my project up and running. 
- [Controls](#controls) shows the controls when playing. 
- [File Contents](#file-contents) explains where all the files are and what they do. 
- [Visualizing NetCDF4 in Unreal Engine](#visualizing-netcdf4-in-unreal-engine) explains in detail each step needed. This should also be useful to modify my code to visualize any other file format, there are already examples for [NPZ](py_rawdata_view/npz_translator.py) and [VDB](py_rawdata_view/Blender/npz_vdb_translator_linux.py) files. 
- [Credits](#credits) list who worked on the project.
- [License](#license) is CC-BY.

If you have any comments or questions, you can send me a message at [alex.kenkenberg@gmail.com](mailto:alex.kenkenberg@gmail.com).


## Updates Post Submission
I spent another year working on my thesis project after submitting it to the LRZ. I made a number of improvements and fixes. 

- VR support
- Better performance
- Smoother animations
- Rain veils and lightning
- Rain particles
- Weather sound effects
- Cloud atmosphere shadows
- Usability improvements
- Performance settings
- Many bug-fixes, probably added some new bugs too


## Setup
This repository runs on Windows, but some optional python scripts need to run in Linux. 

The Python scripts use [Python 3.8](https://www.python.org/downloads/release/python-380/), but newer versions seem to work as well. Depending on the script, you may need to import a handful of libraries. 

The Unreal project works in both [Unreal Engine 4.27](https://github.com/EpicGames/UnrealEngine/releases/tag/4.27.2-release) and [Unreal Engine 5.0](https://github.com/EpicGames/UnrealEngine/releases/tag/5.0.3-release). However, some customization is required, so please refer to the [Unreal Engine 4.27 Setup](#unreal-engine-427-setup) and [Unreal Engine 5.0 Setup](#unreal-engine-50-setup) sections for instructions. 

The volume-texture files containing the cyclone simulation are stored separately on [Open Data LMU](https://doi.org/10.5282/ubm/data.311). Simply download cloud-thesis_ue_textures.zip.001 - 005 and unpack them next to the project folder. 

### Unreal Engine 4.27 Setup
If you don't plan on viewing the volumetric clouds in VR/split-screen, you can simply install Unreal Engine 4.27 via the Epic Launcher and skip the rest of the 4.27 setup. If you are using nDisplay for 3D-screens you don't need a custom engine version either. 

If you want to see the clouds in VR/Split-Screen, you will need to create your own build of 4.27 with the following changes.
- Download the [source code for 4.27](https://github.com/EpicGames/UnrealEngine/releases/tag/4.27.2-release). **If you cannot open the link follow this [guide](https://github.com/EpicGames/Signup) to be able to view the Unreal Engine Github repository.** 
- The following [git commit](https://github.com/EpicGames/UnrealEngine/commit/3808796330293133103b009ac630c716c1de6fc1) of 5.0 also fixes the VR/3D/split-screen bugs for 4.27. Apply the 2 changes from the git commit to your 4.27 source code.
- Follow the instructions in the [Unreal Engine readme](https://github.com/EpicGames/UnrealEngine/tree/4.27.2-release) section to get a customized version of 4.27 running. 
- In the project files, right-click on the [cloud_thesis.uproject](cloud_thesis.uproject) file and select "Switch Unreal Engine Version". Then select the newly created build of 4.27.
- You can now start the project with the [cloud_thesis.uproject](cloud_thesis.uproject) file and VR should work perfectly.
- In the Content Browser on the bottom right click View Options, enable the Show Plugin Content option.
- In the Content Browser, navigate to the OpenXR Content/Devices folder that is now visible. Open the material M_XRController.
- Change the Material Shading Model to Unlit.
- Plug the Base Texture that is plugged into Base Color into Emissive Color. The VR controllers should now be visible and no longer completely black.

#### UE4.27 Cave Setup
This section explains the changes you need to make to the project to build it for Cave on Linux. 
You can use the default version of the UE4.27 engine to build it, but you may need to obtain custom versions of nDisplay, LivelinkOverNDisplay, and DTrack. The following settings in the project must be changed. 

- [DefaultGame.ini](Config/DefaultGame.ini) contains a line "BlueprintNativizationMethod=Inclusive". This line must be deleted. This will slow down the build by ~1ms, but LivelinkOverNDisplay will not build with it enabled. 
- In [DefaultEngine.ini](Config/DefaultEngine.ini) change the line "vr.InstancedStereo=True" to "vr.InstancedStereo=False" because it causes crashes. Of course, you can also turn it off in the editor settings. 
- In the editor settings, change the launch map to either [L_LC_V2C](Content/CustomContent/Maps/Build/L_LC_V2C.umap) or [L_SLC_V2C](Content/CustomContent/Maps/Build/L_SLC_V2C.umap). I would recommend [L_LC_V2C](Content/CustomContent/Maps/Build/L_LC_V2C.umap) as it runs better.

I have automated a few settings. The project assumes that if the level name contains "V2C" it runs in the Cave. The [BP_FlyingPawnVR](Content/CustomContent/Blueprints/Modes/BP_FlyingPawnVR.uasset) has a macro "IsCave", which should help you find any changes if they cause problems. 

### Unreal Engine 5.0 Setup
Unreal Engine 5.0 implementation of volumetric clouds for my system about 1ms faster than the same scene in 4.72. You also don't need to make a custom build to run it in VR. So if nothing else is stopping you, you should build this project in 5.0. At the time of writing, 5.0 is the latest version of the Unreal Engine, but future versions will probably work as well and have more improvements. So it might be a good idea to try the current version of the Unreal Engine. 

To change the engine version, either open Unreal Engine 5.0 and select this project folder. Or right-click on the [cloud_thesis.uproject](cloud_thesis.uproject) file and select "Switch Unreal Engine Version". Then select 5.0 and open the [cloud_thesis.uproject](cloud_thesis.uproject) file.

The only setting you need to change is the antialiasing in the engine settings from TSAA to None. TSAA is very expensive, even if it makes the clouds and hard edges nicer.

If you want to use VR mode, also follow the steps below. 
- In the Content Browser on the top right click Settings, enable the Show Plugin Content option.
- In the Content Browser, navigate to the Engine/Plugins/OpenXR Content/Devices folder that is now visible. Open the material M_XRController.
- Change the Material Shading Model to Unlit.
- Plug the Base Texture that is plugged into Base Color into Emissive Color. The VR controllers should now be visible and no longer completely black.


## Controls
Controls for keyboard and mouse and Xbox controller: 

You can also perform a performance test by pressing the "+" key and an animated performance test by pressing the "#" key. The results are saved in "*Saved\Profiling*" and "*Saved\Screenshots*". Note that this does not work for VR, so you will need to use SteamVR's internal metrics.

![Mouse/Keyboard and Xbox controls](Images/controls_help_bg.png)

<details>
  <summary markdown="span">Controls for HTC Vive controller: </summary>

![HTC Vive controls](Images/controls_help_vr_Vive_bg.png) 
</details>
<details>
  <summary markdown="span">Controls for Oculus Rift controller: </summary>

![Oculus Touch controls](Images/controls_help_vr_Oculus_bg.png)
</details>
<details>
  <summary markdown="span">Controls for Mixed Reality: </summary>

![Wand controls](Images/controls_help_vr_MixedReality_bg.png)
</details>
<details>
  <summary markdown="span">Controls for Cave Wand: </summary>

![Wand controls](Images/controls_help_vr_Wand_bg.png)
</details>

## File Contents
I will explain where each file is located and what it is supposed to do. 

- [py_rawdata_view](py_rawdata_view) contains all Python scripts. 
    - [nc_translator](py_rawdata_view/nc_translator.py) contains the scripts for translating a NetCDF4 file to Volume Textures and for extracting the distance information in textures, CSVs. 
    - [npz_translator](py_rawdata_view/npz_translator.py) contains a variant of nc-translator for NPZ (Numpy Zip) files. 
    - [stats_and_graphs](py_rawdata_view/stats_and_graphs.py) contains all the math for creating graphs and statistics from survey or performance measurement data.
    - [istarmap](py_rawdata_view/istarmap.py) is a utility class for multiprocessing and can be ignored.
    - The [Blender](py_rawdata_view/Blender) folder contains scripts that translate NPZ files to VDB, unfortunately at the time of writing this only runs on Linux because of the VDB library. 
    - [Deprecated](py_rawdata_view/Deprecated) contains a number of abandoned or old versions of scripts.
    - [Experimental](py_rawdata_view/Experimental) contains a number of experimental scripts. 
        - [Cmap_Visualizer](py_rawdata_view/Experimental/Cmap_Visualizer.py) translates matplotlib color maps to CSV, textures, and poly6 functions, so they can be used in various rendering engines. 
        - [curvefitter](py_rawdata_view/Experimental/Cmap_Visualizer.py) is a fairly crude fitting function to approximate spacing information. It requires a lot of help and fiddling. 
        - [textureChannelEditor](py_rawdata_view/Experimental/textureChannelEditor.py) is used to rearrange the texture channels and switch between textures. This is used to switch the texture channels of the translated simulations, as it is much faster to edit PNGs than to reload the original simulation files. 
        - [vectormap](py_rawdata_view/Experimental/vectormap.py) is a visualizer for vector field textures or in my case wind speed stored in a texture. This helps to visualize if the wind animation is playing in the right direction in the Unreal Engine. 

- [Content/CustomContent/Blueprints](Content/CustomContent/Blueprints) contains blueprint scripts.
    - [Curves/C_SampleCount](Content/CustomContent/Blueprints/Curves/C_SampleCount.uasset) resembles a root(x) plot and is used by [BP_CloudDynSample](Content/CustomContent/Blueprints/BP_CloudDynSample.uasset) to adjust cloud parameters based on the elevation percentage. 
    - [Deprecated](Content/CustomContent/Blueprints/Deprecated) contains a set of abandoned or old versions of scripts.
    - [Enums](Content/CustomContent/Blueprints/Enums) contains enums for animation state, controller, and hud.
    - [Modes](Content/CustomContent/Blueprints/Modes) contains the modes used.
        - [BP_FlyingModeVR](Content/CustomContent/Blueprints/Modes/BP_FlyingModeVR.uasset) Handles loading and saving the settings. 
        - [BP_FlyingPawnVR](Content/CustomContent/Blueprints/Modes/BP_FlyingPawnVR.uasset) controls all button inputs, user interface for 2D and VR game modes, and movement. 
        - [BP_SaveGame](Content/CustomContent/Blueprints/Modes/BP_SaveGame.uasset) saves the two customizable settings.
    - [Widgets](Content/CustomContent/Blueprints/WIdgets) contains widgets or blueprints with user interface widgets. 
    - [BP_CloudAnimator_4](Content/CustomContent/Blueprints/BP_CloudAnimator_4.uasset) manages volumetric cloud animation and edits the material as it runs. It loads volumetric textures asynchronously during animation and swaps them in the cloud material when interpolation makes a frame invisible. 
    - [BP_CloudDynSample](Content/CustomContent/Blueprints/BP_CloudDynSample.uasset) dynamically changes a number of parameters in the volumetric clouds at runtime as a function of altitude to significantly improve performance with minimal impact on visual fidelity. It also communicates with BP_CloudAnimator_4 to adjust draw distance.
    - [BP_MetricCube](Content/CustomContent/Blueprints/BP_MetricCube.uasset) creates a number of hollow, unlit cubes in the cardinal directions. It has a customizable label for its size. This can be used to measure the size of the clouds in the levels.
    - [BP_PerfAnalysis](Content/CustomContent/Blueprints/BP_PerfAnalysis.uasset) works with n cameras tagged "PerfCam" to measure performance and save frame time to a CSV file when called during gameplay. This does not work for VR, so you need to use SteamVR's internal metrics. This script is a modified version from this [Inside Unreal guide](https://youtu.be/ZRaeiVAM4LI).
    - [BP_SequencePlayer](Content/CustomContent/Blueprints/BP_SequencePlayer.uasset) is used to play a sequence if it is a level. This is only used to capture scenes for the user study. 
    - [BP_SphereWorldBorder](Content/CustomContent/Blueprints/BP_SphereWorldBorder.uasset) is a rectangular collision box attached to a sphere mesh the size of a planet. The planet rotates every few seconds and looks at the player. This ensures that the player cannot go below the planet's surface, where the clouds and atmosphere break, but also provides a relatively close approximation of the planet's curvature. 
    - [BP_SunSky_Static](Content/CustomContent/Blueprints/BP_SunSky_Static.uasset) is a modified SunSky actor with a SkyAtmosphere. The sun is static for better performance. In deprecated is a dynamically animated version [BP_SunSky_Dynamic](Content/CustomContent/Blueprints/Deprecated/BP_SunSky_Dynamic.uasset) with stars and a moon, but not all performance and fidelity settings in Directional Light and SkyAtmosphere are up-to-date. 
    - [BP_WorldBorder](Content/CustomContent/Blueprints/BP_WorldBorder.uasset) is a cubic collision box designed to prevent players from escaping very large levels at very high speeds. 
    - [BP_WorldOriginPointer](Content/CustomContent/Blueprints/BP_WorldOriginPointer.uasset) is a debug tool that looks like a very large, colorful cross. It teleports to the current world origin every 0.1 seconds. I am rebasing the world origin in [BP_FlyingPawnVR](Content/CustomContent/Blueprints/Modes/BP_FlyingPawnVR.uasset) in VR to avoid jittering and rounding errors at extreme distances. This script helps to track the point of origin. 
    - [BP_Weather](Content/CustomContent/Blueprints/BP_Weather.uasset) manages the particle rain and the weather sounds, it works very closely with [BP_CloudAnimator_4](Content/CustomContent/Blueprints/BP_CloudAnimator_4.uasset). 

- [Content/CustomContent/Cinematics](Content/CustomContent/Cinematics) contains cutscenes used for user study. 
- [Content/CustomContent/Maps](Content/CustomContent/Maps) contains all maps.
    - [Build](Content/CustomContent/Maps/Build) contains one level with large clouds with 255km boundaries and a second level with super large clouds with 2000km boundaries. 
    - [Deprecated](Content/CustomContent/Maps/Deprecated) contains older test versions of clouds and experiments. 
    - [Experiment](Content/CustomContent/Maps/Experiment) contains the maps used for experiments, but in the current version on git the maps for [trueSky](https://simul.co/) and [Emelianov](https://www.artstation.com/artwork/GXnrvV) clouds have been removed to avoid build problems. 
    - [L_playground](Content/CustomContent/Maps/L_playground.umap) is my current test map. It is more or less identical to the [super large build map](Content/CustomContent/Maps/Build/L_SuperLarge.umap). 
- [Content/CustomContent/Materials](Content/CustomContent/Materials) contains all my materials and material functions.
    - [Experimental](Content/CustomContent/Materials/Experimental) contains experiments with materials. Most are 2D variants of components in volumetric clouds, which are easier to test in 2D. 
        - [DirectVolume](Content/CustomContent/Materials/Experimental/DirectVolume) contains unlit material variants of volumetric clouds that resemble classic scientific visualizations of volumetric data. The deprecated folder contains older iterations. 
    - [Final](Content/CustomContent/Materials/Final) contains the materials used for the experiments in my paper. Many of them are materials where a component was disabled to study its impact on performance. These materials are deprecated and the latest variant is [m_9_Cloud](Content/CustomContent/Materials/Prototypes/m_9_Cloud.uasset). 
    - [Generic](Content/CustomContent/Materials/Generic) contains basic, very simple 2D materials. 
    - [Prototypes](Content/CustomContent/Materials/Prototypes) contains prototypes of clouds. A lower number means that it is an earlier version. 
        - [Experiments](Content/CustomContent/Materials/Prototypes/Experiments) contains material instances of [m_9_Cloud](Content/CustomContent/Materials/Prototypes/m_9_Cloud.uasset) used for testing. 
        - [M_4_Baked_Voronoi_Clouds](Content/CustomContent/Materials/Prototypes/M_4_Baked_Voronoi_Clouds.uasset) This is the latest variant that uses a mesh to create clouds. These clouds look similar to those in the game [Sky: Children of the Light ](https://youtu.be/aBQRCHusYyU).
        - [m_9_Cloud](Content/CustomContent/Materials/Prototypes/m_9_Cloud.uasset) is the latest version of my volumetric cloud material. It is used in the build version. The big difference from version 8 is motion vector blending for the cloud shape, a lot of procedural components that replace textures, added rain and lightning. It also has a lot of switches to turn off each component. 
- [Content/CustomContent/Particles](Content/CustomContent/Particles) contains a basic particle mist used for a very old experiment and a simple rain particle effect. 
- [Content/CustomContent/Shapes](Content/CustomContent/Shapes) contains meshes for a lot of older prototypes. Most of them are unused.
- [Content/CustomContent/Sound](Content/CustomContent/Shapes) contains audio files for mostly weather effects. They modified tracks from the [GameAudioGDC](https://sonniss.com/gameaudiogdc) sound bundle. 
- [Content/CustomContent/Textures](Content/CustomContent/Textures) contains, as the name implies, my textures. The very large files that contain the simulations are not in the git repository. The files are stored separately on [Open Data LMU](https://doi.org/10.5282/ubm/data.311). Simply download cloud-thesis_ue_textures.zip.001 - 005 and unpack them inside the project folder. 
    - [1Cloud_2h](Content/CustomContent/Textures/1Cloud_2h) contains the files for a small test simulation that looks suspiciously like a mushroom-cloud. The textures are not in the git repository. The files are stored separately on [Open Data LMU](https://doi.org/10.5282/ubm/data.311). Simply download cloud-thesis_ue_textures.zip.001 - 005 and unpack them next to the project folder. 
    - [Baking](Content/CustomContent/Textures/Baking) has procedural textures of noise and the tools to create more. This [tutorial](https://www.unrealengine.com/en-US/tech-blog/getting-the-most-out-of-noise-in-ue4) explains how it works. 
    - [ColorMaps](Content/CustomContent/Textures/ColorMaps) is filled with curve color maps, mostly from [Python Matplotlib](https://matplotlib.org/stable/tutorials/colors/colormaps.html), used for testing or for the unlit cloud materials.
    - [CustomTextures](Content/CustomContent/Textures/CustomTextures) contains a number of test textures and some UI textures for the help screen, plus the moon and star textures I painted. 
    - [qc](Content/CustomContent/Textures/qc) contains a set of 2D and volumetric test textures generated from the CM1 simulations. Some of them are wrong because the vectors point in the wrong direction or are upside down, so you should use the correct ones from [rawdata_view](Content/CustomContent/Textures/rawdata_view) instead. 
    - [rawdata_view](Content/CustomContent/Textures/rawdata_view) contains all textures created from the CM1 model. This is the smaller 512 x 512 x 32 variant with only the central area. It also contains a set of color curves or texture variant for straight line scaling of the texture. I recommend not using the curves or textures, as using a [fitted function](Content/CustomContent/Textures/rawdata_view_6k/MF_cm1out_Z_FitFunc.uasset) is much faster. The textures are not in the git repository. The files are stored separately on [Open Data LMU](https://doi.org/10.5282/ubm/data.311). Simply download cloud-thesis_ue_textures.zip.001 - 005 and unpack them next to the project folder. 
    - [rawdata_view](Content/CustomContent/Textures/rawdata_view_6k) has all the textures generated from the CM1 model. This is the larger 1024 x 1024 x 32 variant with the entire simulation. It also contains a set of color curves or a texture variant for straight line scaling of the texture. However, I recommend a customized function for the [XY](Content/CustomContent/Textures/rawdata_view_6k/MF_cm1out_XY_FitFunc.uasset) and [Z](Content/CustomContent/Textures/rawdata_view_6k/MF_cm1out_Z_FitFunc.uasset) dimensions, as they are much faster textures and curves, respectively. The textures are not in the git repository. The files are stored separately on [Open Data LMU](https://doi.org/10.5282/ubm/data.311). Simply download cloud-thesis_ue_textures.zip.001 - 005 and unpack them next to the project folder. 
    - [A_ScaleAtlas](Content/CustomContent/Textures/A_ScaleAtlas.uasset) is a collection of all my color curves that needs to be called to access the curves in a material. 


## Visualizing NetCDF4 in Unreal Engine

### How to analyze NetCDF4 files? 
How do you find out how to set your parameters for translating? This works for a single NC file or multiple files in a folder. **Do all the steps even if you have multiple files to save a lot of time.**   
    
1. Open [nc_translator.py](py_rawdata_view/nc_translator.py).
2. Scroll down to the *main* function. Examples of everything you need can be found here. Comment out everything in the *main* function, since we don't need it yet.  
3. First use the *nc_scan* function to read a single NC file to get variable names, descriptions and their forms. Like this:
    ```python
    nc_scan("example.nc") # Replace "example.nc" with a NetCDF4 file you want to scan
    ```  
    
    The other parameters don't matter at the moment, because we don't know yet what is in our NC file.

4. The script creates an *example.txt* next to the NC file you scanned. Open the *example.txt* file. 
5.  We are interested in the section *VARIABLES*. The smaller solid lines separate different variables within the *VARIABLES* section.  Example of two variables from the *VARIABLES* section:
    ```html
    <class 'netCDF4._netCDF4.Variable'>
	float32 qc(time, nk, nj, ni) <br />
		def: mixing ratio <br />
		units: kg/kg <br />
		x_min: -1495.2501 <br />
		y_min: -1495.2501 <br />
		z_min: 0.025 <br />
	unlimited dimensions: time <br />
	current shape = (1, 40, 1140, 1140) <br />
	filling on, default _FillValue of 9.969209968386869e+36 used <br />
		qc max: 0.0022919436 <br />
		qc min: 0.0 <br />
		qc empty: [0, 32, 33, 34, 35, 36, 37, 38, 39] <br />
	______________________________________ 
	<class 'netCDF4._netCDF4.Variable'> <br />
	float32 qr(time, nk, nj, ni) <br />
		def: mixing ratio <br />
		units: kg/kg <br />
		x_min: -1495.2501 <br />
		y_min: -1495.2501 <br />
		z_min: 0.025 <br />
	unlimited dimensions: time <br />
	current shape = (1, 40, 1140, 1140) <br />
	filling on, default _FillValue of 9.969209968386869e+36 used <br />
		qr max: 0.0124425925 <br />
		qr min: 0.0 <br />
    ```

6. Each section we call variable is a description of a variable and its properties. In the examples above we see two variables, one named *qc* and one named *qr*. Select the variables that will later be converted to be imported into Unreal Engine and contain the information used to create volumetric clouds. I used the following variables in the Unreal Engine project: cloud condensate ratio (cloud density) *qc*, water drop ratio (rain density) *qr*, wind speed vectors (wind vectors) *uinterp*, *vinterp*, *winterp*. 
7. If you have only one NC file, you can continue with the [translation](#how-to-translate-netcdf4-files-to-png-image-files) section.
8. If you have multiple NC files, comment out the *nc_scan* function and use the *multi_nc_scan* function within the same .py file. This function scans all NC files in a directory and gives you a text file similar to the single file scan function. It contains the global minimum and maximum. It also lists the z-slices of the image that are empty for the variable *qc*. It is possible to set the minimum and maximum values for *qc*, depending on how they are set, the selection of empty z-slices increases. Create an array containing all the string names of the variables you want to export to save time and skip those you are not interested in. An example could be:
    ```python
    multi_nc_scan("exampleDirectory", ['qc', 'qr', 'uinterp', 'vinterp', 'winterp'], vmin=0.0001, vmax=0.0028)
    ```
   
9. A new CSV and a text file named *nc_folder_scan* will be created in the folder you just scanned. It contains the global maxima and minima and the global *qc empty*. These values will be used in the next section. 
    

### How to translate NetCDF4 files to PNG image files?
Once you have scanned the .nc* files, you can translate an NC file. The *nc_to_png* function can translate a single NC file. Translating NC files can take a few seconds to minutes and CM1 simulations can contain hundreds of NC files. So even if you want to translate all NC files in a simulation, it is best to test your settings with one file first. I will explain the parameters you need and how to construct them from the information collected with *nc_scan* and *nc_multi_scan*.  

1. Return to the script *nc_translation.py* and browse to the function *main*.
2. Comment everything except the *nc_to_png* function and the parameters of the large simulation. Example parameters for the simulations I translated are in the *parameters small simulation* and *parameters large simulation* sections. If you want to use my material without changes, follow these two examples and save the cloud and rain density in one texture and the wind speed in the RGB channel of a second texture. 
3. I will go into the parameters of the large simulation as an example:
    ```python
    flip = (0, 2)  # flip Z and X !!! remember to invert the corresponding Vectors !!!
    min_velocity, max_velocity = -50, 50
    invertfunc = functools.partial(invert) 
    minusfunc = functools.partial(minus, 0.005)
    dict_libary = {
        'qc': {'func': None, 'vmin': 0.0001, 'vmax': 0.0028},  # Cloud density
        'qr': {'func': minusfunc, 'vmin': 0.0, 'vmax': 0.005},  # Rain density
        'uinterp': {'func': None, 'vmin': min_velocity, 'vmax': max_velocity},  # X wind velocity, --1 = 1
        'vinterp': {'func': invertfunc, 'vmin': min_velocity, 'vmax': max_velocity},  # y wind velocity
        'winterp': {'func': None, 'vmin': min_velocity, 'vmax': max_velocity}}  # Z wind velocity --1 = 1
    selection = (1, 314, 314, 32, 512, 512)  # selection to have all data and 4k res
    png_library = [{'variables': ['qc', 'qr', None], 'bit': 8, 'selection': selection},
                   {'variables': ['uinterp', 'vinterp', 'winterp'], 'bit': 8, 'selection': selection}]
    ```
   
    Let's proceed from the bottom up. The *png_library* is a list of dictionaries. Each dictionary creates a PNG file. In this example, two PNG files are created. One 8-bit PNG file with cloud density (qc) in the red channel, rain density (qr) in the blue channel, and an empty green channel. And a second 8-bit PNG with the three wind vectors in the RGB channels. You can store up to four variables in a PNG and also switch to 16-bit colors. 
    The *selection* variable is a section of the NC file you want to extract. It is a tuple of six integers describing a 3D rectangle. The first three are the upper left corner and the other three are the length. You can find the shape of your cloud density variable in the text file under *current shape*. Usually you don't want to extract everything from the NC files because they are much larger than necessary. You can use *qc empty* to select the height of your selection, since the slices listed are all empty. Remember that the *power of two* rule also applies to volume textures and each of their tiles. The translator will automatically make the output texture a power of two and fill the rest of the texture with 0. This can be disabled with the *base2* flag in the function. 
    Next comes the *dict_libary* which contains information about the variables.  In the text files for the variables you will find the maximum and minimum. If you have multiple NC files, you must use the global minimum and maximum variables of the specific variables from *nc_folder_scan.txt*. The *func* stands for a partial function you want to apply to the values. 
    The *flip* is a list of axes to flip the data on. For NC in Unreal Engine, you need to flip the Z and X axes. If you mirror any axes and also extract vectors, remember to invert the vectors for the non-mirrored axes, otherwise the vectors will point in the wrong direction.
4. Enter the parameters in the function and execute the script, for example, as follows:
    ```python
    nc_to_png(input_filepath, png_library, png_path=output_filepath, dict_lib=dict_libary, flip=flip)
    ```
   
5. Check if the output files are correct. Translating can take a lot of time and it would be a waste to do it several times if one parameter is set incorrectly. 
6. If all parameters are correct, you can now translate all NC files in a folder. The parameters are the same, the input file is simply replaced by an input folder, such as:
    ```python
    nc_to_png(input_filepath, png_library, png_path=output_filepath, dict_lib=dict_libary, base2=True, flip=flip)
    ```

### How to extract the spacing information as CSV files from NetCDF4 files?
The Unreal Engine uses a linear coordinate system. This means that the distance from 0-1 is equal to the distance from 1-2. Unfortunately, the NC files do not always follow the same system. These steps explain how to extract the distances between values as CSV files. If you already know that your NC files have linear spacing, then you can skip this step. If you don't know, follow these steps and you will figure it out. 
1. The coordinate system should be the same for one data set, so we only need to scan one *.nc* file. Open the text file from the first scan in this guide *example.txt*. It should contain all the variables. Look for the X, Y, Z *location of the scalar grid points*. In my files the variables were called *xh*, *yh*, *z*. 
2. Create a dictionary of the variables with the selection of your corresponding png files so that the dimensions overlap exactly. An example could be:
    ```python
    save_curve("example.nc", {'xh': (314, 512),'yh': (314, 512), 'z': (1, 32)})
    ``` 
   
3. Three CSV files should have been created in addition to the *.nc* file. In addition, 3 to 4 image files have been created. The file name contains important information. After the original file name comes first the variable, second the minimum value and third the length of the original values. The minimum and the length are needed to scale the textures to their original size. The units should be in the description of your *example.txt*, in my case kilometers.
4. If you want to check if the values are linearly distributed, open the CSV files and check if the values in both columns are identical. You could also open the file in Excel and create a chart from both columns. If it is a straight diagonal line, the data is linearly distributed and you don't have to worry about the distance information for that axis. However, check the CSV files for all axes as they may be different.

### How to import PNG files as a volume texture into Unreal Engine 4.26?
I will explain the steps on how to import hundreds of images into Unreal Engine as volume textures with the correct settings. 

1. Open my project in Unreal Engine. Open the *Plugins* menu. Make sure "*Python Editor Script Plugin*" is enabled. 
2. In the content browser, create a new folder for each type of PNG file you created with the translation script. In my example, I created two folders. One for the cloud density and rain textures and another for the wind textures. 
3. Open Windows Explorer and the folder where your texture files are located. 
4. Select all the textures of one type in Explorer and drag them to the folder in the Content Browser that you just created for them. Do this for all your texture types.
5. Open the Python file [py_rawdata_view/UnrealEngine/createVolumeTex.py](py_rawdata_view/UnrealEngine/createVolumeTex.py) in an editor of your choice. The script will translate all the selected textures in the Unreal Engine content browser into volume textures and apply all the settings to them. Read the code carefully to see if these settings are the ones you want. I've linked the documentation for the other compression types so you can add more. 
6. We need to edit the script slightly to make sure the settings are correct for the type of textures you are importing. Scroll down to the *main* function at the end. It is filled with examples that I used for my project. Comment everything out first. 
7. Write the *transfrom_selected* function in the *main*, for example like this:
    ```python
    transfrom_selected('/Game/CustomContent/Textures/rawdata_view/qc_qr_v/', colorchannel='BC7', downscale=None, mips=False, tilesize=512)
    ```   
    
    The first parameter is the folder path where the volume textures should be saved. It should be a separate path for each texture type. Note that the path is a bit strange. It always starts with "/Game". This means the *content* folder of your project.
    The *color-channel* can be set to *None* for standard DXT1, '*GS*' for grayscale, or '*Vector*' for vector shift compression or '*BC7*' for BC7 texture compression. If you need other compression settings, you must add them to the code. The names of the compression settings are listed [here](https://docs.unrealengine.com/4.26/en-US/PythonAPI/class/TextureCompressionSettings.html#unreal.TextureCompressionSettings).
    *Downscale* is *None* if you want to keep the texture size. If you want to reduce the maximum size, you can put an integer here for the maximum size of a tile. If you want to use mips, you can enable the flag. Tile-size is the size of the XY dimensions for a tile in a PNG. It should be written in the filename of the PNGs. 
8. Now return to the Unreal Engine Editor. Select the textures you want to translate in the Content Browser. Then switch to the output log and write the following command, but change the path so that it displays the *createVolumeTex.py* file on your system:
    ```bash
    py "D:\cloud-thesis\py_rawdata_view\UnrealEngine\createVolumeTex.py"
    ```
   
9. If you have a lot of textures and want to save some space and reduce the loading time for volume textures, you can perform these optional steps. Once all volume textures are created, close the Unreal Engine. In File Explorer, go into your Unreal project and delete the .uasset files for the 2D textures. All information is stored in both the 2D textures and the volume textures. The 2D textures are referenced in the volume textures and are always loaded as well. However, they are only needed if you want to easily re-import the files. 
10. You can now open the Unreal Engine project again. Unreal Engine will give warnings when you open the volume textures because they reference files that do not exist. Select as many volume textures as your memory can handle at once, and move the cursor to make the editor load them into memory. This will freeze your editor for a while, so be patient and wait. Then press ctrl+s to save all the selected volume textures. The reference for the 2D source texture will be set to empty, and you should no longer receive any warnings. Repeat this process with all volume textures. 

   
### How to import spacing information into Unreal Engine?
This step is only necessary if you have at least one axis that is not linearly spaced. Here you have 3 options. You can use textures, functions or color curves. 

You can use the textures you created with *save_curve* to change the UVs of the volume textures to match the spacing information for each axis. You must set the compression settings for the textures to Half Float (R16) or you will get compression artifacts and strange cubic artifacts in the clouds. This is the easiest method, but the 2-3 texture lookups required are bad for performance.

It is best to recreate the function that created the curve in the first place. This is much faster than a texture lookup (0.7-2ms faster for me). You can use polynomial fitting with the distance information from the CSVs generated by *save_curve* to do this. But for more complex functions, you need to support the fitting function and generate partial values of the graph yourself. See here an [example](https://www.desmos.com/calculator/e51szjtie4) in Desmos and my [script for fitting](py_rawdata_view/Experimental/curvefitter.py). 

If you have a very small cloud (~50km or less), you can also generate color curves from the csv files. But this is also as slow as using 3 texture samples:
1. Drag the CSV files from Windows Explorer into the Unreal Engine content browser. 
2. Import them as *Linear Color Curve* and ignore the warning about too few cells. 
3. Create a duplicate of the X-axis curve. We will store all curves of a simulation in the color channels of the one curve. 
4. Open the duplicate. The X-axis is already saved in the red channel. Open the next curve for the Y-axis and select and copy the nodes of the curve. Now select a green color channel in the duplicated curve and paste the node of the Y-axis. Repeat this for the Z-axis and the blue color channel. 5.
5. Change the name of the duplicate so that the information of the other axes is also stored in it, so that you don't forget which axis is stored where and how long it was originally. 
6. If the curve for one axis is straight, it has a linear distance, and you should use "Multiply" to scale the texture back to the original size. This is faster and the curve will produce some visual artifacts if scaled too large. If it's not straight, I'll explain how to use it in the next section. 

### How to visualize the imported simulation in a level
You should now have a set of volume textures and perhaps some sort of spacing modification. We can visualize it now. I have two example levels in the [Content/CustomContent/Maps/Build](Content/CustomContent/Maps/Build) folder. If you have translated your files similar to me, you can simply use my materials and scripts to visualize them. I will explain how to do this. 
1. The current prototype is [m_9_Cloud](Content/CustomContent/Materials/Prototypes/m_9_Cloud.uasset). It has a lot of switches and parameters. All of them are commented so you can edit them. There are also a number of material instances with examples. 
2. If your textures are rectilinear, you need to enable Rectilinear Map. Depending on the rectilinear dimension enable the switches for those axes. If you have a function, you will need to edit the material code itself to implement it, otherwise you can use textures and color curves for each dimension (remember that textures are ~1ms slower than functions). If you want to use the rain particles, you need to create a simple renter target material for the weather sample mat slot in [BP_CloudAnimator_4](Content/CustomContent/Blueprints/BP_CloudAnimator_4.uasset). It simply reads the color values at a specific UV coordinate in your cloud textures. It also needs the same rectilinear displacement your clouds get. 
4. Set *Map Tex* and *Map Tex 2* to have two cloud density and rain textures, where *Map Tex 2* is exactly one index further than *Map Tex*. Repeat the same for the wind and rain textures with *Flow Tex* and *Flow Tex*. For example, you will have the textures of image number 1 in the first slots and the textures of image number 2 in the second slot. 
5. Open the [Content/CustomContent/Maps/Build/L_LargeCloud.umap](Content/CustomContent/Maps/Build/L_LargeCloud.umap) map. Select the *VolumetricCloud* actor in the layer. Change the *Layer Bottom Altitude* to the minimum Z-axis value to be stored in the curve name. Change the *Layer Height* to the length value of the Z-axis, which should also be saved in the name of the curve. 6. 
6. Drag and drop the new material instance into the *Material* slot of the *VolumetricCloud*. You should now be able to see your cloud in the level. Note that many settings, including the material and size, are overwritten when playing [BP_CloudAnimator_4](Content/CustomContent/Blueprints/BP_CloudAnimator_4.uasset) and [BP_CloudDynSample](Content/CustomContent/Blueprints/BP_CloudDynSample.uasset), so check their parameters before editing the volumetric cloud parameters. 
7. If you have wind velocity vectors, make sure you have *flow interpolation* enabled. Then change the *Flow Strength* parameter so that when you change the *Alpha* of the interpolation, one frame transitions smoothly to the next without jittering. 
8. Change the parameters of your new material instance until you are happy with its appearance. They're all commented and should give you an idea of what they do. 
9. Let's set up the animation for multiple frames so you can see all the snapshots of your simulation. In the Unreal Engine, go to Edit/Project Settings/Asset Manager. There should already be a set of *Primary Asset Types to Scan* from my experiments that you can use as examples. 
10. Create a new *Primary Asset Types to Scan* and give it a name of your choice. Set the *Primary Asset Type* to *Volume Texture*. Add a *Directory*. Set the path to the folder containing your cloud textures. The path works the same as in the Python script you used to create volume textures, so you should be able to copy it. The *Content* folder is called */Game*. Use the examples as a reference if you get confused.
11. Repeat the same for the wind texture folder. 
12. Return to the level and select [BP_CloudAnimator_4](Content/CustomContent/Blueprints/BP_CloudAnimator_4.uasset). Change *Asset Type Cloud* to the new asset type you just created for the cloud textures folder. Repeat the same for *Asset Type Wind* for the Wind folder. 
13. Change the *Material List* so that your cloud instance is on the first index. You can add more cloud instances to switch between them at runtime.
14. You can change the *Sec Per Frame* and *Starting Index Percentage* values to your liking. 
15. Press Play. Normally *ESC* or *F1* will open the controls window, but since it is in the editor, it will stop playback. You can use a controller and press *Start* to see all the controls. 


##  Credits
This master project was written by [Alexander Kenkenberg](mailto:alex.kenkenberg@gmail.com). <br />
The guiding tutor was [Daniel Kolb](mailto:Daniel.Kolb@lrz.de). <br />
The CM1 simulations were made by [Gerard Kilroy](https://www.meteo.physik.uni-muenchen.de/~gerard.kilroy/) for the paper ["A unified view of tropical cyclogenesis and intensification"](https://www.meteo.physik.uni-muenchen.de/~gerard.kilroy/Publications/M10a.pdf) <br />

## License 
This work is licensed under a [Creative Commons Attribution 4.0 International License][cc-by].

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

