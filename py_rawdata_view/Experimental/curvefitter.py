import os  # read directories
import numpy as np
from pandas import read_csv
from scipy.optimize import curve_fit
from matplotlib import pyplot


def linear(x, a, b):
    return a * x + b


def poly2(x, a, b, c):
    return a * x + b * x ** 2 + c

def poly3(x, a, b, c, d):
    return (a * x) + (b * x ** 2) + (c * x ** 3) + d


def poly5(x, a, b, c, d, e, f):
    return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (e * x ** 5) + f


def sinfunc(x, a, b, c, d, e):
    return a * np.sin((b - x) * c) + d * x ** 2 + e


def my_headfunc(x, a, b):
    k = max(0., min(0.424, x))
    return a * (b * (k - 0.424)) ** (1 / 1.99999) + 0.205


def poly6(x, a, b, c, d, e, f, g):
    """ is a simple polynomial function with a*x + b*x^2 + ... + f*x^6 + g """
    # return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (e * x ** 5) + (f * x ** 5) + g
    return g + x * (f + x * (e + x * (d + x * (c + x * (b + x * a)))))


# http://sigmaquality.pl/uncategorized/fit-curve-to-data_-scipy-optimize-curve_fit-en220120201529/
def myFit(function, x, y):
    # curve fit
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
    popt, _ = curve_fit(function, x, y)

    # plot input vs output
    pyplot.scatter(x, y)
    # define a sequence of inputs between the smallest and largest known inputs
    minX, maxX = min(x), max(x)
    countX = (maxX - minX) / 255
    x_line = np.arange(minX, maxX, countX)

    if function == linear:
        # summarize the parameter values
        a, b = popt
        print('y = %.5f * x + %.5f' % (a, b))
        # calculate the output for the range
        y_line = function(x_line, a, b)
    elif function == poly2:
        # summarize the parameter values
        a, b, c = popt
        print('y = %.5f * x + %.5f * x^2 + %.5f' % (a, b, c))
        # calculate the output for the range
        y_line = function(x_line, a, b, c)
    elif function ==poly3:
        # summarize the parameter values
        a, b, c, d = popt
        print('y = %.5f * x + %.5f * x^2 + %.5f * x^3+ %.5f' % (a, b, c, d))
        # calculate the output for the range
        y_line = function(x_line, a, b, c, d)
    elif function == poly5:
        # summarize the parameter values
        a, b, c, d, e, f = popt
        print('y = %.5f * x + %.5f * x^2 + %.5f * x^3 + %.5f * x^4 + %.5f * x^5 + %.5f' % (a, b, c, d, e, f))
        # calculate the output for the range
        y_line = function(x_line, a, b, c, d, e, f)
    elif function == sinfunc:
        # summarize the parameter values
        a, b, c, d, e = popt
        print('y = %.5f * sin((%.5f - x)* %.5f) + %.5f * x^2  +  %.5f' % (a, b, c, d, e))
        # calculate the output for the range
        y_line = function(x_line, a, b, c, d, e)
    elif function == my_headfunc:
        # summarize the parameter values
        a, b = popt
        print('y =%.5f *(%.5f * k)^(1/3) + 0.205 with k=x-0.424' % (a, b))
        # calculate the output for the range
        y_line = function(x_line, a, b)
    elif function == poly6:
        a, b, c, d, e, f, g = popt
        print(
            '%.5f +x*(%.5f +x*(%.5f +x*(%.5f +x*(%.5f +x*(%.5f +x*%.5f)))))'
            % (g, f, e, d, c, b, a))
        y_line = poly6(x_line, a, b, c, d, e, f, g)
    else:
        print('function not found')
        y_line = x_line

    # create a line plot for the mapping function
    pyplot.plot(x_line, y_line, '--', color='red')
    pyplot.show()


def printxy(x, y):
    text = ''
    for i in range(len(x)):
        text += '(' + str(x[i]) + ',' + str(y[i]) + '),'
    return text


def main():
    # filepath to csv files

    # y = 0.14815 * x + 0.85185 * x ^ 2
    z_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                          'rawdata_view', 'cm1out', 'cm1out_001030_z_m0.09_r16.63.csv')

    # Inverse y = 4.89682 * x + -20.89406 * x^2 + 44.97599 * x^3 + -46.56993 * x^4 + 18.62797 * x^5 + -0.01840
    xh_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                           'rawdata_view', 'cm1out', 'cm1out_001030_xh_m-1000.53_r2001.05.csv')

    # y = 3.91212 * x + -1.45606
    xh_linear_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                  'rawdata_view', 'xh_m-1000.53_r2001.05_LinearOnly.csv')

    xh_exp_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                               'rawdata_view', 'xh_m-1000.53_r2001.05_ExpOnly.csv')

    # y = 0.48247 * x + -3.89401 * x^2 + 28.32060 * x^3 + -82.87856 * x^4 + 88.81755
    # inverted y = 3.86464 * x + -8.74891 * x^2
    xh_head_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                'rawdata_view', 'xh_m-1000.53_r2001.05_headOnly.csv')

    # y = 180.19905 * x + -445.56266 * x^2 + 550.46752 * x^3 + -338.81735 * x^4 + 83.04777 * x^5 + -28.3321
    # inverted y = -13.63316 * x + 8.74890 * x^2 + 5.88426
    xh_tail_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                'rawdata_view', 'xh_m-1000.53_r2001.05_tailOnly.csv')

    xh_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                           'rawdata_view', 'donut_approx.csv')

    # load the dataset
    dataframe = read_csv(xh_path, header=None, skiprows=1)
    data = dataframe.values
    x, y = data[:, 0], data[:, 4]

    x = x[np.logical_not(np.isnan(y))]
    y = y[np.logical_not(np.isnan(y))]  # prune nans
    # print(printxy(x, y))

    # y, x = data[:, 0], data[:, 1]  # inverted graph
    myFit(poly5, x, y)


if __name__ == "__main__":
    # execute only if run as a script
    main()
