import matplotlib.pyplot as plt
import matplotlib.colors as clr
import numpy as np
import os  # read directories
import png  # to save as png
from scipy.optimize import curve_fit
import imageio  # image reader

def rescale_x_bit(data, bit=8, vmin=None, vmax=None):
    """
    takes any np array. and rescales it to use the full bit range of integer values.
    :param data: data array of numbers
    :param bit: bit range of 8, 16 or 32
    :param vmin: minimum value to be set the new 0, values that were smaller will also be 0
    :param vmax: maximum value, values that are larger will be capt at the maximum
    :return: returns np array with 16bit int of the data shape
    """

    # check if bit range is acceptable
    if bit != 8 and bit != 16 and bit != 32:
        raise RuntimeError('only select bit range of 8, 16 or 32')

    # minus 1 because we start from 0
    bit_range = (2 ** bit) - 1

    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the int16 limit or under
        data = data.clip(min=vmin, max=vmax)

    if value_range == 0:  # to avoid division by 0
        rescaled_data = np.zeros(np.shape(data), dtype=data.dtype)  # everything will be set 0 as well
    else:
        rescaled_data = (bit_range * ((data - vmin) / value_range))

    # set datatype to the proper bit range
    if bit == 8:
        rescaled_data = rescaled_data.astype(np.uint8)
    elif bit == 16:
        rescaled_data = rescaled_data.astype(np.uint16)
    else:
        rescaled_data = rescaled_data.astype(np.uint32)

    return rescaled_data

def read_png(inputfile):
    """
    reads a png file
    :param inputfile: filepath to the png file
    :return: array in the shape (channel, y dimension, x dimension)
    """
    # load image
    image = imageio.imread(inputfile)

    # # reorder the image to a uniform shape
    # if len(np.shape(image)) < 3:
    #     # greyscale images
    #     image = [image]
    # else:
    #     # every other image
    #     image = np.transpose(image, (2, 0, 1))

    return image


def plotRGBGraph(x, y, y2=None, filepath=None, color="rgb", title=None):
    """
    make plot of Red Green Blue Color Graph
    :param x: location of the y values
    :param y: array of R G B channels for plotting
    :param y2: optional second set of y values used by poly fitter
    :param filepath: optional filepath so graph will be saved instead of shown
    :param color: optional RGB or HSV so y will be translated to the HSV color storing method
    :param title: optional title
    :return:
    """
    fig, axs = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 10]})

    # cmap over the line graph
    axs[0].imshow([y], aspect='auto')
    if title is not None:
        axs[0].text(-0.01, 0.5, title, va='center', ha='right', fontsize=10, transform=axs[0].transAxes)
    axs[0].set_axis_off()

    # linegraph showing rgba values
    if color == 'RGB' or color == 'rgb' or color is None:
        axs[1].plot(x, y[:, 0], label="Red", c='r')
        axs[1].plot(x, y[:, 1], label="Green", c='g')
        axs[1].plot(x, y[:, 2], label="Blue", c='b')
        # axs[1].plot(x, y[:, 3], label="Alpha", c='k')  # Alpha is never used
    elif color == 'HSV' or color == 'HSB' or color == 'hsv' or color == 'hsb':
        y_hsv = clr.rgb_to_hsv(y[:, 0:3])
        axs[1].plot(x, y_hsv[:, 0], label="Hue", c='c')
        axs[1].plot(x, y_hsv[:, 1], label="Saturation", c='r')
        axs[1].plot(x, y_hsv[:, 2], label="Value", c='dimgray')
        axs[1].plot(x, y[:, 3], label="Alpha", c='k')

    if y2 is not None:
        # create a line plot for the mapping function
        axs[1].plot(x, y2[0], '--', color='k')
        axs[1].plot(x, y2[1], '--', color='k')
        axs[1].plot(x, y2[2], '--', color='k')

    axs[1].set_xlim(0, 1)
    axs[1].set_ylim(0, 1)
    axs[1].set_xlabel("Width")
    axs[1].set_ylabel("Color Value")

    # better gridlines
    major_ticks = np.linspace(0, 1, 10 + 1)
    minor_ticks = np.linspace(0, 1, 20 + 1)
    axs[1].set_xticks(major_ticks)
    axs[1].set_xticks(minor_ticks, minor=True)
    axs[1].set_yticks(major_ticks)
    axs[1].set_yticks(minor_ticks, minor=True)
    axs[1].grid(which='both')
    axs[1].grid(which='minor', alpha=0.2)
    axs[1].grid(which='major', alpha=0.5)

    fig.tight_layout()
    plt.legend()

    if filepath is None:
        plt.show()
    else:
        plt.savefig(filepath)
    plt.close()


def plotcmap(cmap_name, filepath=None, color="rgb", samples=256):
    """
    plot cmap as an RGB color graph
    :param samples: number of samples the graph makes for each colormap
    :param color: color system you want (rgb, hsv)
    :param cmap_name: matplotlib cmap name https://matplotlib.org/2.0.2/examples/color/colormaps_reference.html
    :param filepath: optional  file location ending in .png if none is set it will plt.show
    :return:
    """
    x = np.linspace(0, 1, samples)
    y = plt.get_cmap(cmap_name)(x)

    plotRGBGraph(x, y, filepath=filepath, color=color, title=cmap_name)


def NormalizeData(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))


def plot1DPNG(pngpath, filepath=None):
    """
    makes rgb graph for a 1 dimensional PNG (Horizontal)
    :param pngpath: png file path
    :param filepath: optional outputpath if you want to save the plot
    :return:
    """
    _, title = os.path.split(pngpath)  # make title of filename

    y = read_png(pngpath)[0]
    y = NormalizeData(y)  # rescale to 0-1 range

    x = np.linspace(0, 1, len(y))  # 0-1 range for each pixel of png

    plotRGBGraph(x, y, filepath=filepath, title=title)  # plot rgb graph


def save_png(data, outputfile, bit=8):
    """
    saves a up to 4 2 dimensional arrays into a png
    :param data: array in the shape (1-4, X, Y) filled with integers from 0 - (2^bit)-1
        Note:   1 2D array will make a greyscale image
                2 2D arrays will make a grayscale image with alpha
                3 2D arrays will make a RGB image
                4 2D arrays will make a RGBA image
    :param outputfile: path of the png file
    :param bit: 8 or 16 for 8 or 16 bit per channel
    :return:
    """

    if data.__len__() > 4:
        raise RuntimeError('No more than 4 variables per PNG. PNGs have only four channels.')
    elif data.__len__() == 1:
        greyscale, alpha = True, False
        png_data = data[0]
    elif data.__len__() == 2:
        greyscale, alpha = True, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    elif data.__len__() == 3:
        greyscale, alpha = False, False
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    else:
        greyscale, alpha = False, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])

    # Use pypng to write z as a color PNG.
    with open(outputfile, 'wb') as f:
        writer = png.Writer(width=np.shape(data[0])[1], height=np.shape(data[0])[0],
                            bitdepth=bit, greyscale=greyscale, alpha=alpha)
        writer.write(f, png_data)


def cmapToCSV(cmap_name, filepath, samples=512):
    """
    saves cmap as csv for inmport into unreal engine
    :param samples: number of samples the graph makes for each colormap
    :param zeroisnull: make values of zero also have a color value of 0
    :param cmap_name: matplotlib cmap name https://matplotlib.org/2.0.2/examples/color/colormaps_reference.html
    :param filepath: file location ending in .csv
    :return:
    """
    x = np.linspace(0, 1, samples)
    y = plt.get_cmap(cmap_name)(x)

    data = np.concatenate((np.array([x]).T, y), axis=1)
    np.savetxt(filepath, data, delimiter=",", fmt='%f')


def cmapToTexture(cmap_name, filepath, samples=1024, bit=16):
    """

    :param cmap_name:
    :param filepath:
    :param samples:
    :param bit:
    :return:
    """
    x = np.linspace(0, 1, samples)
    y = plt.get_cmap(cmap_name)(x)

    data = np.array([[y[:, 0]], [y[:, 1]], [y[:, 2]], [y[:, 3]]])  # join them
    data = rescale_x_bit(data, bit=bit)  # rescale them to the color range of image

    save_png(data, filepath, bit=bit)


def poly6(x, a, b, c, d, e, f, g):
    """ is a simple polynomial function with a*x + b*x^2 + ... + f*x^6 + g """
    # return (a * x) + (b * x ** 2) + (c * x ** 3) + (d * x ** 4) + (e * x ** 5) + (f * x ** 5) + g
    return g + x * (f + x * (e + x * (d + x * (c + x * (b + x * a)))))


def GetFitValuesPoly6(x, popt):
    """ Small helper function that prints the poly6 and returns its y values """
    # summarize the parameter values
    a, b, c, d, e, f, g = popt
    print(
        '%.5f +x*(%.5f +x*(%.5f +x*(%.5f +x*(%.5f +x*(%.5f +x*%.5f)))))'
        % (g, f, e, d, c, b, a))
    y_fit_values = poly6(x, a, b, c, d, e, f, g)
    return y_fit_values


def rgbToPoly(x,y, filepath=None, show=True, title=None):
    poptR, _ = curve_fit(poly6, x, y[:, 0])  # red values
    poptG, _ = curve_fit(poly6, x, y[:, 1])  # green values
    poptB, _ = curve_fit(poly6, x, y[:, 2])  # blue values

    if filepath is not None:
        # saving as text file to be used elsewhere
        csvlist = np.flip(np.vstack((poptR, poptG, poptB)).T, 0)
        np.savetxt(filepath, csvlist, delimiter=",", fmt='%f')

    y_fit_valuesR = GetFitValuesPoly6(x, poptR)
    y_fit_valuesG = GetFitValuesPoly6(x, poptG)
    y_fit_valuesB = GetFitValuesPoly6(x, poptB)

    if show:
        y2 = [y_fit_valuesR, y_fit_valuesG, y_fit_valuesB]
        plotRGBGraph(x, y, y2=y2, title=None)

def cmapToPoly(cmap_name, filepath=None, samples=512, show=True):
    """
    Fits as best it can a polynomial 6 function to a colormap. Note that some just don't work very well.
    :param cmap_name: The string name of the Matplotlib Colormap
    :param filepath: Optional Filepath if you wish to have the paramters saved as a text file
    :param samples: number of samples of the colormap
    :param show: if you wish to see the poly6 compates to the orignial colormap
    :return: -
    """

    # makes x 0-1 number range, y are the values from the cmap
    x = np.linspace(0, 1, samples)
    y = plt.get_cmap(cmap_name)(x)  # note this returns an array of RGBA floats

    rgbToPoly(x, y, filepath=filepath, show=show, title=cmap_name)


def png1DToPoly(pngpath, filepath=None, show=True):
    _, title = os.path.split(pngpath)  # make title of filename

    y = read_png(pngpath)[0]
    y = NormalizeData(y)  # rescale to 0-1 range

    x = np.linspace(0, 1, len(y))  # 0-1 range for each pixel of png

    rgbToPoly(x, y, filepath=filepath, show=show, title=title)  # plot rgb graph


def main():
    # https://matplotlib.org/2.0.2/examples/color/colormaps_reference.html
    # visualizes a cmap as a color graph so it can be copied in unreal
    cmap_name = 'rainbow'
    pngpath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                           'CustomTextures', 'UE_1D_Sun2.png')
    filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                            'Colormaps', 'C_cmap_' + cmap_name)

    # plot1DPNG(pngpath)
    png1DToPoly(pngpath, show=True)
    # cmapToPoly(cmap_name, show=True)  # , filepath + '_poly6.txt')
    # plotcmap(cmap_name)
    # plotcmap(cmap_name, filepath + '.png')
    # cmapToCSV(cmap_name, filepath + '.csv')

    # save all colormaps in all available formats, may take a minute or two
    cmap_list = plt.colormaps()  # all cmaps of matplotlib
    cmap_list = ['afmhot', 'bone', 'cividis', 'CMRmap', 'copper', 'cubehelix', 'gist_earth', 'gist_rainbow',
                 'gnuplot2', 'hsv', 'inferno', 'jet', 'magma', 'pink', 'rainbow', 'twilight', 'twilight_shifted',
                 'viridis']
    # for cmap_name in cmap_list:
    #     filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
    #                             'Colormaps', 'C_cmap_' + cmap_name)
    #     plotcmap(cmap_name, filepath + '_graph.png')
    #     cmapToCSV(cmap_name, filepath + '.csv')
    #     cmapToTexture(cmap_name, filepath + '.png')
    #     cmapToPoly(cmap_name, filepath + '_poly6.txt', show=False)


if __name__ == "__main__":
    # execute only if run as a script
    main()
