# for Python 3.8
import os  # read directories
import matplotlib.pyplot as plt  # plots
import numpy as np  # math + arrays
import imageio  # image reader
from mpl_toolkits import mplot3d

def rebin(a, shape):
    sh = shape[0], a.shape[0] // shape[0], shape[1], a.shape[1] // shape[1]
    return a.reshape(sh).mean(-1).mean(1)


def plotflowmap(png_path):
    """
    This generates a plot that visualizes a vector map for the red and green channel in a png image.
    :param png_path: string to the png
    :return:
    """

    im = imageio.imread(png_path)

    red_channel = im[:, :, 0]
    green_channel = im[:, :, 1]
    blue_channel = im[:, :, 2]

    # rescale the vectors from 0-255 color value to a -1 to 1 float
    U = -2 * ((red_channel / 255) - 0.5)
    V = -2 * ((green_channel / 255) - 0.5)

    # gridlines
    im_s = np.shape(im)
    print(im_s)
    X, Y = np.meshgrid(np.linspace(0, im_s[1], im_s[1]), np.linspace(0, im_s[0], im_s[0]))

    # vector length
    M = np.hypot(U, V)

    # small version for only 64x64 vectors and not millions
    small = (32, 32)
    U_small = rebin(U, small)
    V_small = rebin(V, small)
    X_small, Y_small = np.meshgrid(np.linspace(0, im_s[1], small[1]), np.linspace(0, im_s[0], small[0]))

    fig, ax = plt.subplots(figsize=(12, 12))

    # shows flowmap
    ax.imshow(im)
    # stream line plot with 90% deg rotation of vector (U, V) => (Y, -X)
    ax.streamplot(X, Y, V, -U, linewidth=2 * M, density=5, arrowstyle='-', color='k')
    # if imshow is in the same graph Vsmall has to be inverted. I have no clue why
    ax.quiver(X_small, Y_small, U_small, -V_small, pivot='mid', scale=1 / 0.05, width=0.003, color='w')

    plt.axis('equal')
    plt.axis('off')
    plt.tight_layout()
    # plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
    plt.show()

    # https://www.numbercrunch.de/blog/2013/05/visualizing-streamlines/
    # https://scipy-cookbook.readthedocs.io/items/LineIntegralConvolution.html


def main():
    png_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                            'rawdata_view', 'cm1out_001030_uinterp_vinterp_winterp_x512_y512.png')
    # png_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow',
    #                           'sim_000070', '000150_velocity__x64_y128_z64.png')

    plotflowmap(png_path)

if __name__ == "__main__":
    # execute only if run as a script
    main()
