# for Python 3.8
import sys  # to change print write to log and not console
import os  # read directories
import numpy as np  # nc imports and np
import png  # to save as png
import imageio  # image reader
import multiprocessing as mp  # for multiprocessing
import istarmap  # import to apply patch to multiprocessing, NOTE!!! this is used don't trust hints
from tqdm import tqdm  # create progressbar


def list_files_with_ext(directory, ext):
    """
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    """
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist


def save_png(data, outputfile, bit=8):
    """
    saves a up to 4 2 dimensional arrays into a png
    :param data: array in the shape (1-4, X, Y) filled with integers from 0 - (2^bit)-1
        Note:   1 2D array will make a greyscale image
                2 2D arrays will make a grayscale image with alpha
                3 2D arrays will make a RGB image
                4 2D arrays will make a RGBA image
    :param outputfile: path of the png file
    :param bit: 8 or 16 for 8 or 16 bit per channel
    :return:
    """
    # test if outputfile folderpath exist otherwise create it
    outputfolder = os.path.split(outputfile)[0]
    if not os.path.exists(outputfolder):
        os.mkdir(outputfolder)

    if data.__len__() > 4:
        raise RuntimeError('No more than 4 variables per PNG. PNGs have only four channels.')
    elif data.__len__() == 1:
        greyscale, alpha = True, False
        png_data = data[0]
    elif data.__len__() == 2:
        greyscale, alpha = True, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    elif data.__len__() == 3:
        greyscale, alpha = False, False
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    else:
        greyscale, alpha = False, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])

    print(np.shape(data[0])[1], np.shape(data[0])[0], bit, greyscale, alpha)
    # Use pypng to write z as a color PNG.
    with open(outputfile, 'wb') as f:
        writer = png.Writer(width=np.shape(data[0])[1], height=np.shape(data[0])[0],
                            bitdepth=bit, greyscale=greyscale, alpha=alpha)
        writer.write(f, png_data.copy())


def read_png(inputfile):
    """
    reads a png file
    :param inputfile: filepath to the png file
    :return: array in the shape (channel, y dimension, x dimension)
    """
    # load image
    image = imageio.imread(inputfile)

    # reorder the image to a uniform shape
    if len(np.shape(image)) < 3:
        # greyscale images
        image = [image]
    else:
        # every other image
        image = np.transpose(image, (2, 0, 1))

    return image


def rescale_x_bit(data, bit=8, vmin=None, vmax=None):
    """
    takes any np array. and rescales it to use the full bit range of integer values.
    :param data: data array of numbers
    :param bit: bit range of 8, 16 or 32
    :param vmin: minimum value to be set the new 0, values that were smaller will also be 0
    :param vmax: maximum value, values that are larger will be capt at the maximum
    :return: returns np array with 16bit int of the data shape
    """

    # check if bit range is acceptable
    if bit != 8 and bit != 16:
        raise RuntimeError('only select bit range of 8, 16')

    # minus 1 because we start from 0
    bit_range = (2 ** bit) - 1

    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the int16 limit or under
        data = data.clip(min=vmin, max=vmax)

    if value_range == 0:  # to avoid division by 0
        rescaled_data = np.zeros(np.shape(data), dtype=data.dtype)  # everything will be set 0 as well
    else:
        rescaled_data = (bit_range * ((data - vmin) / value_range))

    # set datatype to the proper bit range
    if bit == 8:
        rescaled_data = rescaled_data.astype(np.uint8)
    else:
        rescaled_data = rescaled_data.astype(np.uint16)

    return rescaled_data


def all_equal(iterator):
    """
    check is a list is filled with identical elements
    :param iterator: any list
    :return: boolean true for all are identical
    """
    return len(set(iterator)) <= 1


def correctImageToLowestIntType(image):
    """
    # correct channel bit depth ot the lowest bit depth of the channels
    :param image: array in the shape (channel, y dimension, x dimension)
    :return: image channels with contents the same data type
    """
    # check the channel bit depth of the image
    channelbitdepth = []
    for channel in image:
        channeldatatype = type(channel[0][0])

        if channeldatatype == np.uint8:
            bitdepth = 8
        elif channeldatatype == np.uint16:
            bitdepth = 16
        elif channeldatatype == np.uint32:
            bitdepth = 32

        channelbitdepth.append(bitdepth)

    # check if all bit depths are equal otherwise translate them to the smallest
    if not all_equal(channelbitdepth):
        mindepth = min(channelbitdepth)

        newimage = []
        for index, channel in enumerate(image):
            bitdepth = channelbitdepth[index]
            if bitdepth == mindepth:
                # do nothing if the channel equal the smallest bit depth
                newimage.append(channel)
            else:
                # rescale to the images smallest bit depth if one channel is larger
                newchannel = rescale_x_bit(channel, bit=mindepth, vmin=0, vmax=(2 ** bitdepth) - 1)
                newimage.append(newchannel)

        image = newimage  # replace original

    return image


def rearrangeIndex(infolders, outfolder, reorder, infiles_list, index):
    """
    helper function for multiprocessing for rearangePNGs
    :param infolders: list of input folders
    :param outfolder: list of output folders
    :param reorder: [[(index image source, index image channel)]] A list of lists of tuples.
        The tuples are filled with the index of their parent folder and then the index of the source channel. These
        tuples are in a list of up to four that will make up a new png. These lists of tuples are in another list that
        make up a list of output images. Setting the channel to None means to just have all values values set to 0.
    :param infiles_list: list of the folder contents
    :param index: current index of which images to work on.
    :return: nothing but saves a png file
    """

    # read all images of an index in
    inpng_List = []
    for index_folder in range(len(infolders)):
        image = read_png(infiles_list[index_folder][index])
        inpng_List.append(image)

    # apply and save the transfromations of the reorder list
    for index_2, imageInstruction in enumerate(reorder):
        newImage = []
        for channel in imageInstruction:

            if channel[1] is None:
                # if source channel is set to None fill the channel with zeros
                sourceImage = inpng_List[channel[0]]
                channelshape = np.shape(sourceImage[0])
                sourceChannel = np.zeros(channelshape, dtype=np.uint8)
            else:
                # get the source image and the specified channel
                sourceImage = inpng_List[channel[0]]
                sourceChannel = sourceImage[channel[1]]

            # add in order to the new image
            newImage.append(sourceChannel)

        # correct channel bit depth
        newImage = correctImageToLowestIntType(newImage)

        # first index is of the index of the image in the folders, second index is of the different shuffled images
        imageOutfolder = os.path.join(outfolder[index_2], str(index_2) + "_" + "{:03d}".format(index) + ".png")
        save_png(newImage, imageOutfolder)
        print(imageOutfolder)


def rearangePNGs(infolders, outfolder, reorder):
    """
    translates all pngs in the inputfolders according the instructions in reorder to new pngs in the outputfolder
    :param infolders: list of input folders
    :param outfolder: list of output folders
    :param reorder: [[(index image source, index image channel)]] A list of lists of tuples.
        The tuples are filled with the index of their parent folder and then the index of the source channel. These
        tuples are in a list of up to four that will make up a new png. These lists of tuples are in another list that
        make up a list of output images. Setting the channel to None means to just have all values values set to 0.
    :return: nothing but saves a png file
    """
    # if the reordering list doesn't match the output folders put them all in the first folder
    if len(reorder) != len(outfolder):
        outfolder = np.repeat(outfolder[0], len(reorder))

    # read all files from the input folder
    infiles_list = []
    for infolder in infolders:
        imagefiles = list_files_with_ext(infolder, 'png')
        # imagefiles = np.flip(imagefiles)  # for testing so he does the last image first
        infiles_list.append(imagefiles)

    to_do_list = []
    for index in range(len(infiles_list[0])):
        # builds a list that has all the arguments for each process
        to_do_list.append((infolders, outfolder, reorder, infiles_list, index))
        # break  # for testing so only one image set gets translated

    with mp.Pool(processes=8) as pool:
        # pool.starmap(nc_to_png, to_do_list)  # without progressbar
        for _ in tqdm(pool.istarmap(rearrangeIndex, to_do_list), total=len(to_do_list)):  # adds progressbar for mp
            pass


def main():
    # makes it so that cloud density and rain density and an empty blue channel make one texture
    # the second texture is filled with the wind vectors
    reorder = [[(0, 0), (1, 3), (0, None)], [(1, 0), (1, 1), (1, 2)]]
    qc_3k_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                  'rawdata_view', 'qc_3k_16bit')
    uinterp_vinterp_winterp_qr_3k_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master',
                                                          'Assets', 'rawdata_view', 'uinterp_vinterp_winterp_qr_3k')
    output_folderpaths_3k = [os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                          'rawdata_view', 'qc_qr_3k'),
                             os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                          'rawdata_view', 'uinterp_vinterp_winterp_3k')]
    # rearangePNGs([qc_3k_filepath, uinterp_vinterp_winterp_qr_3k_filepath], output_folderpaths_3k, reorder)

    qc_6k_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                  'rawdata_view', 'qc_6k_16bit')
    uinterp_vinterp_winterp_qr_6k_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master',
                                                          'Assets', 'rawdata_view', 'uinterp_vinterp_winterp_qr_6k')
    output_folderpaths_6k = [os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                          'rawdata_view', 'qc_qr_6k'),
                             os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                          'rawdata_view', 'uinterp_vinterp_winterp_6k')]
    rearangePNGs([qc_6k_filepath, uinterp_vinterp_winterp_qr_6k_filepath], output_folderpaths_6k, reorder)


if __name__ == "__main__":
    # execute only if run as a script
    main()
