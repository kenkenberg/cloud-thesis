# for Python 3.8
import sys  # to change print write to log and not console
import os  # read directories
import netCDF4 as nc  # to read netCDF4 files from simulations
import numpy as np  # nc imports and np
import math  # sqrt and such
import png  # to save as png
import multiprocessing as mp  # for multiprocessing
import istarmap  # import to apply patch to multiprocessing, NOTE!!! this is used don't trust hints
from tqdm import tqdm
import vtk
import pandas as pd  # dataframe for better editing of data tables

def list_files_with_ext(directory, ext):
    """
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    """
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist


def rescale_x_bit(data, bit=8, vmin=None, vmax=None):
    """
    takes any np array. and rescales it to use the full bit range of integer values.
    :param data: data array of numbers
    :param bit: bit range of 8, 16 or 32
    :param vmin: minimum value to be set the new 0, values that were smaller will also be 0
    :param vmax: maximum value, values that are larger will be capt at the maximum
    :return: returns np array with 16bit int of the data shape
    """

    # check if bit range is acceptable
    if bit != 8 and bit != 16:
        raise RuntimeError('only select bit range of 8, 16 or 32')

    # minus 1 because we start from 0
    bit_range = (2 ** bit) - 1

    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the int16 limit or under
        data = data.clip(min=vmin, max=vmax)

    if value_range == 0:  # to avoid division by 0
        rescaled_data = np.zeros(np.shape(data), dtype=data.dtype)  # everything will be set 0 as well
    else:
        rescaled_data = (bit_range * ((data - vmin) / value_range))

    # set datatype to the proper bit range
    if bit == 8:
        rescaled_data = rescaled_data.astype(np.uint8)
    else:
        rescaled_data = rescaled_data.astype(np.uint16)

    return rescaled_data


def tile_array(data, base2=True):
    """
    takes 3d array and tiles all the slices of the z dimension next to each other in a 2d array
    :param data: 3d data array of the shape (z, y, x)
    :param base2: if true the output array as well as its tiles will have a shape of a multiple of 2
    :return: 2d np array
    """

    # calculate size of 2d array
    (z_length, y_length, x_length) = np.shape(data)  # NOTE!!! the order of the Z,Y,X given by np.shape

    if base2:  # calculating size of base to image and its tiling
        new_x_length, new_y_length = 2, 2
        while x_length > new_x_length:  # image with base 2 to fit one tile in x length
            new_x_length = new_x_length * 2
        while y_length > new_y_length:  # image with base 2 to fit one tile in y length
            new_y_length = new_y_length * 2

        new_x_tiling_length, new_y_tiling_length = new_x_length, new_y_length
        z, x_offset, y_offset = 0, 0, 0
        while z < z_length:  # check if all tiles fit into image
            x_offset += new_x_tiling_length  # next column
            if x_offset > (new_x_length - new_x_tiling_length):  # new row
                if new_x_length <= new_y_length:
                    new_x_length += new_x_length  # expand image to the side, because we are out of space
                else:
                    new_y_length += new_y_length  # expand image below, because we are out of space
                    x_offset = 0  # back to column 0
                    y_offset += new_y_tiling_length
            z = math.ceil((new_x_length / new_x_tiling_length) * (new_y_length / new_y_tiling_length))  # update z
        new_data = np.zeros((new_y_length, new_x_length), dtype=data.dtype)
    else:
        number_of_rows = math.ceil(math.sqrt(z_length))
        new_x_tiling_length, new_y_tiling_length = x_length, y_length
        new_x_length, new_y_length = number_of_rows * x_length, number_of_rows * y_length
        new_data = np.zeros((new_y_length, new_x_length), dtype=data.dtype)

    # place the slices of the 3d array in the new empty 2d array
    x_offset, y_offset = 0, 0  # where we place the next image

    for z in range(z_length):
        # place array (x and y are flipped cause im dumb, but has to stay like this)
        new_data[y_offset:y_offset + y_length, x_offset:x_offset + x_length] = data[z]
        x_offset += new_x_tiling_length  # next column
        if x_offset > (new_x_length - new_x_tiling_length):
            x_offset = 0  # back to column 0
            y_offset += new_y_tiling_length  # new row

    number_of_tile = math.ceil(new_y_length / new_y_tiling_length) * math.ceil(new_x_length / new_x_tiling_length)
    # squared col_and_row_number because the other tiles will be empty but needed for proper scaling
    shape = (number_of_tile, new_x_tiling_length, new_y_tiling_length)

    return new_data, shape


def save_png(data, outputfile, bit=8):
    """
    saves a up to 4 2 dimensional arrays into a png
    :param data: array in the shape (1-4, X, Y) filled with integers from 0 - (2^bit)-1
        Note:   1 2D array will make a greyscale image
                2 2D arrays will make a grayscale image with alpha
                3 2D arrays will make a RGB image
                4 2D arrays will make a RGBA image
    :param outputfile: path of the png file
    :param bit: 8 or 16 for 8 or 16 bit per channel
    :return:
    """

    if data.__len__() > 4:
        raise RuntimeError('No more than 4 variables per PNG. PNGs have only four channels.')
    elif data.__len__() == 1:
        greyscale, alpha = True, False
        png_data = data[0]
    elif data.__len__() == 2:
        greyscale, alpha = True, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    elif data.__len__() == 3:
        greyscale, alpha = False, False
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    else:
        greyscale, alpha = False, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])

    # Use pypng to write z as a color PNG.
    with open(outputfile, 'wb') as f:
        writer = png.Writer(width=np.shape(data[0])[1], height=np.shape(data[0])[0],
                            bitdepth=bit, greyscale=greyscale, alpha=alpha)
        writer.write(f, png_data)


def npz_to_png(folder_path, file_number, png_library, dict_lib, png_path=None, base2=True, flip=None):
    """
    opens volumetric npz files of the file_number and translates them into png for unreal engine
    :param folder_path: path to the folder containing the number
    :param file_number: filenumber plus extionsion
    :param png_library: a list of dictionaries for each png, in the form:
        [{'variables': [string], 'bit': int, 'selection': (in, int, int, int, int, int)}]
        variables: string of the variable name in the npz file, 'qc' for example
        bit = int 8, 16 for the the bit depth per channel of the image
        selection: tuple of six integers describing a square (x, y, width, height)
            z coordinate of the square, corner
            x coordinate of the square, corner
            y coordinate of the square, corner
            height: height of the square
            width: of the square
            depth: of the square
    :param dict_lib: dictionary of the form:
        {variable_name: {'flip': Bool, 'func': func, 'vmin': Int, 'vmax': Int, 'bit': Int}}
        variable_name: string name of variable in the beginning of the npz file
        func: partialfunction or none to apply to variable
        vmin: smallest value that should be black, everything smaller will also be black (not affected by invert!)
        vmax: largest value that should be white, everything larger will also be white (not affected by invert!)
    :param png_path: optional output path of the file. default is same name and dir as original nc file
    :param base2: optional bool, True if the texture should have a resolution base 2. Note, if yours selection is not
        also base 2 ue4 will not generate mip maps making this switch pointless.
    :param flip: optional None = no flip, else a list with the axis to flip (0, 2) to flip Z and Y axis (0=Z, 1=X, 2=Y)
    :return:
    """

    for png_dict in png_library:
        if png_dict['variables'].__len__() > 4:
            raise RuntimeError('No more than 4 variables per PNG. PNGs have only four channels.')

    # check if png is being used, else save png with next to original
    if (png_path == '') or (png_path is None):
        png_path = os.path.splitext(os.path.join(folder_path, file_number))[0] + '.png'

    # load the files for the file_number
    datastructure = {}
    for name in dict_lib:
        data = np.load(os.path.join(folder_path, name + file_number))
        data = data[data.files[0]]  # normal strange operations to get the the actual array inside
        datastructure[name] = np.transpose(data, (3, 1, 0, 2))  # turning the inner array layer outside and rightside up

    # for all the pngs to create
    for png_dict in png_library:

        shape = ''
        png_data = []
        for variable in png_dict['variables']:

            # some variables have more than one array in them, so lets go over all of them
            for index in range(np.shape(datastructure[variable])[0]):

                # read data from structure maybe trim with selection cube
                if png_dict['selection'] is None:
                    data = datastructure[variable][index, :, :, :]
                else:
                    (z, x, y, height, width, depth) = png_dict['selection']
                    data = datastructure[variable][index, z: (z + height), x:(x + width), y: (y + depth)]

                # flip values
                if flip is not None:
                    data = np.flip(data, flip)  # reverse order

                # invert values
                if dict_lib[variable]['func'] is not None:
                    data = dict_lib[variable]['func'](data)

                # tile 3D array to a 2D array
                tiled_data, shape = tile_array(data, base2=base2)

                # after the tiling so 0 will also become scales and flow_maps will not have black edges
                # rescale array to be 8bit int range
                bit_data = rescale_x_bit(tiled_data, bit=png_dict['bit'], vmin=dict_lib[variable]['vmin'],
                                         vmax=dict_lib[variable]['vmax'])

                # add color to png_group
                png_data.append(bit_data)

        # channel names as string
        channel_names = ''
        for variable in png_dict['variables']:
            channel_names += '_' + variable

        # shape to string
        shape_name = '_x' + str(shape[1]) + '_y' + str(shape[2]) + '_z' + str(shape[0])

        # insert dimensions into path
        outputpath = os.path.splitext(png_path)[0] + channel_names + shape_name + os.path.splitext(png_path)[1]

        # save png
        save_png(png_data, outputpath, bit=png_dict['bit'])
        # print('saved to ', outputpath)


def multi_npz_to_png(folder_path, png_library, png_path=None, dict_lib=None, base2=True, flip=None):
    """
    translates all npz files that are mentioned in the dict_lib in folder into pngs.
    It will skip npz files that have translated pngs already.
    :param folder_path: folder that contains .nc (netcdf files)
    :param png_library: a list of dictionaries for each png, in the form:
        [{'variables': [string], 'bit': int, 'selection': (in, int, int, int, int, int)}]
        variables: string of the variable name in the npz file, 'qc' for example
        bit = int 8, 16 for the the bit depth per channel of the image
        selection: tuple of four integers describing a square (x, y, width, height) !CAN NOT be larger than dataset
            z coordinate of the square, corner
            x coordinate of the square, corner
            y coordinate of the square, corner
            height: height of the square
            width: of the square
            depth: of the square
    :param png_path: directory where the output pngs should be stored, default is in same folder as nc files
    :param dict_lib: dictionary of the form:
        {variable_name: {'flip': Bool, 'func': func, 'vmin': Int, 'vmax': Int, 'bit': Int}}
        variable_name: string name of variable in npz file
        func: partial function or none to apply to variable
        vmin: smallest value that should be black, everything smaller will also be black (not affected by invert!)
        vmax: largest value that should be white, everything larger will also be white (not affected by invert!)
    :param base2: bool, True if the texture should have a resolution base 2. Note, if yours selection is not also base
        2 ue4 will not generate mip maps making this switch pointless.
    :param flip: None for no flip, else a list with the axis to flip (0, 2) to flip Z and Y axis (0=Z, 1=X, 2=Y)
    :return:
    """

    # check if output_path is used, if so create the current output file name
    if png_path is not None:
        # test if png_path exist otherwise create it
        if not os.path.exists(png_path):
            os.mkdir(png_path)
    else:  # else set it to folderpath
        png_path = folder_path

    # find all pngs in goal dir and filter for only filename
    existing_png_list = np.array(list_files_with_ext(png_path, '.png'))
    existing_png_list = [os.path.splitext(os.path.basename(file_path))[0] for file_path in existing_png_list]
    png_variables = np.concatenate([x['variables'] for x in png_library])

    # make list of all the nc files in folder dir
    file_list = list_files_with_ext(folder_path, '.npz')

    first_variable = list(dict_lib.keys())[0]  # first variable so all numbers come up only once
    # filter numbers and duplicates out
    file_list = [item.replace(first_variable, '') for item in file_list if first_variable in item]

    to_do_list = []
    for file_path in file_list:
        filename = os.path.splitext(os.path.basename(file_path))[0]

        output_file_path = os.path.join(png_path, filename + '.png')
        if len(existing_png_list):  # if pngs were found
            # lists all index of occurrences of the filename in the existing_png_list
            index_occurrence = np.flatnonzero(np.core.defchararray.find(existing_png_list, filename) != -1)
            if len(index_occurrence):  # if he found any matches
                # fetches filename with index and joins them for easy search
                matching_pngs = ' '.join(np.take(existing_png_list, index_occurrence))
                if all([x in matching_pngs for x in png_variables]):  # if ALL of the png_variables are in the matches
                    print('skipping', filename)
                    continue  # we can safely skip this file

        file_number = filename + '.npz'

        # builds a list that has all the arguments for each process
        to_do_list.append((folder_path, file_number, png_library, dict_lib, output_file_path, base2, flip))

    # runs mutiprossessing for every file in the list of files
    with mp.Pool(processes=8) as pool:
        # pool.starmap(nc_to_png, to_do_list)  # without progressbar
        for _ in tqdm(pool.istarmap(npz_to_png, to_do_list), total=len(to_do_list)):  # adds progressbar for mp
            pass
    # print('finished converting ', folder_path)


def main():
    """parameters ScalarFlow"""
    flip = (1)  # apparently only the y axis is upside down, while the values are already correct

    # source for stats https://ge.in.tum.de/publications/2019-scalarflow-eckert/
    dict_libary = {
        'velocity': {'func': None, 'vmin': -5, 'vmax': 5},  # Wind Velocity
        'density': {'func': None, 'vmin': 0.0, 'vmax': 70},  # Cloud density
    }

    selection = None  # just leave the default resolution or larger of base2 demands it
    # selection = (5, 18, 18, 128, 64, 64)  # smaller just to make some space

    bit = 16
    # png_library = [{'variables': ['velocity', 'density'], 'bit': bit, 'selection': selection}]
    png_library = [{'variables': ['density'], 'bit': bit, 'selection': selection}]

    input_folderpath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow',
                                    'sim_000070')
    file_number = '_000150.npz'
    output_folderpath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow',
                                     'sim_000070', str(bit) + '-bit')

    """Single NPZ to PNG"""
    # read npz files for a number and transform them to png
    # npz_to_png(input_folderpath, file_number, png_library, dict_lib=dict_libary, base2=True, flip=flip)

    """Multi NPZ to PNG"""
    # read all nc files in folder and transform it to png
    multi_npz_to_png(input_folderpath, png_library, png_path=output_folderpath, dict_lib=dict_libary,
                     base2=True, flip=flip)


if __name__ == "__main__":
    # execute only if run as a script
    main()
