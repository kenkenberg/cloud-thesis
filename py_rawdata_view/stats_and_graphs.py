import sys  # to change print write to log and not console
import os  # read directories
import numpy as np  # nc imports and np
import math  # sqrt and such
import pandas as pd  # dataframe for better editing of data tables
import matplotlib.pyplot as plt  # plots
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from statsmodels.stats.weightstats import ztest
import nc_translator
import imageio  # image reader
import scipy.stats
from PIL import Image  # image loader

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)  # to make pandas warnings shut up

def get_survey_data(results_path):
    data_path = os.path.join(results_path, 'data_cloud21_2021-05-14_13-29.csv')
    values_path = os.path.join(results_path, 'values_cloud21_2021-05-14_13-12.csv')
    variables_path = os.path.join(results_path, 'variables_cloud21_2021-05-14_13-26.csv')

    data_df = pd.read_csv(data_path, encoding='ANSI')
    values_df = pd.read_csv(values_path, encoding='ANSI')
    variables_df = pd.read_csv(variables_path, encoding='ANSI')

    for index, row in data_df.iterrows():  # for every line in list
        for i in range(32):  # for every compare question
            indexname = "%02d" % (i + 1)
            if row['RC01x' + indexname] == 2:  # if we use the switched question

                # because the sides of the answers are switched 1->2 and 2->1, 3 remains unchanged
                if row['CS' + indexname] == 3:
                    data_df.at[index, 'CC' + indexname] = 3
                elif row['CS' + indexname] == 2:
                    data_df.at[index, 'CC' + indexname] = 1
                else:
                    data_df.at[index, 'CC' + indexname] = 2

    # delete the first person (lmu mail supervisor) doing the survey because he clearly didn't read it.
    data_df = data_df.drop(0)

    # extract only interesting columns
    columns = ['CASE', 'REF',
               'BI01', 'BI01_06', 'BI02', 'BI02_01', 'BI02_02', 'BI02_03', 'BI02_09', 'BI02_07', 'BI02_04', 'BI02_04a',
               'BI03', 'BI04',
               'CC01', 'CC02', 'CC03', 'CC04', 'CC05', 'CC06', 'CC07', 'CC08', 'CC09', 'CC10', 'CC11', 'CC12', 'CC13',
               'CC14', 'CC15', 'CC16', 'CC17', 'CC18', 'CC19', 'CC20', 'CC21', 'CC22', 'CC23', 'CC24', 'CC25', 'CC26',
               'CC27', 'CC28', 'CC29', 'CC30', 'CC31', 'CC32',
               'CE01', 'CE01x01', 'CE01x02', 'CE01x03', 'CE01x04', 'CE01x05',
               'CE02', 'CE02x01', 'CE02x02', 'CE02x03', 'CE02x04', 'CE02x05',
               ]
    filtered_data_df = data_df.filter(items=columns).copy()

    # save sorted df as csv
    filtered_data_df.to_csv(os.path.join(results_path, 'filtered.csv'))

    original_stdout = sys.stdout
    with open(os.path.join(results_path, 'simple_stats.txt'), 'w') as f:
        sys.stdout = f  # makes print write to file

        """how many from each source"""
        source = 'REF'
        source_counts = filtered_data_df[source].value_counts()
        source_counts.name = 'COUNT'
        source_precentile = filtered_data_df[source].value_counts(normalize=True)
        source_precentile.name = 'PERCENTILE'
        source_counts_df = pd.concat([source_counts, source_precentile], axis=1)

        print('QUESTION: ', source)
        print('ANSWERS:')
        print(source_counts_df)
        print('__________________________________________________')

        """how many of each sex ?"""
        sex = 'BI04'
        sex_df = values_df.loc[values_df['VAR'] == sex].copy()
        sex_counts = filtered_data_df[sex].value_counts()
        sex_df['COUNT'] = sex_df['RESPONSE'].map(sex_counts)
        print('QUESTION: ', variables_df.loc[variables_df['VAR'] == sex]['QUESTION'].values[0])
        print('ANSWERS:')
        print(sex_df.filter(items=['MEANING', 'COUNT']))
        print('__________________________________________________')

        """how many of each age group ?"""
        age = 'BI03'
        age_df = values_df.loc[values_df['VAR'] == age].copy()
        age_counts = filtered_data_df[age].value_counts()
        age_df['COUNT'] = age_df['RESPONSE'].map(age_counts)
        print('QUESTION: ', variables_df.loc[variables_df['VAR'] == age]['QUESTION'].values[0])
        print('ANSWERS:')
        print(age_df.filter(items=['MEANING', 'COUNT']))
        print('__________________________________________________')

        plt.subplots(figsize=(7, 4))
        plt.bar(age_df['MEANING'], age_df['COUNT'])
        plt.ylabel('Amounts')
        plt.xticks(rotation=30, ha='right')
        plt.grid(axis='y')
        plt.tight_layout()
        plt.savefig(os.path.join(results_path, 'bar_age.pdf'))

        """how many of each education group ?"""
        education = 'BI01'
        education_df = values_df.loc[values_df['VAR'] == education].copy()
        education_counts = filtered_data_df[education].value_counts()
        education_df['COUNT'] = education_df['RESPONSE'].map(education_counts)

        print('QUESTION: ', variables_df.loc[variables_df['VAR'] == education]['QUESTION'].values[0])
        print('ANSWERS:')
        print(education_df.filter(items=['MEANING', 'COUNT']))
        # other degree answer
        education2 = 'BI01_06'
        print('"Other degree" Answers:')
        print(filtered_data_df.loc[filtered_data_df[education] == 6][education2])

        # correct table by hand
        education_df = education_df.drop(5).copy()  # drop the other degree row
        education_df.at[1, 'COUNT'] = education_df['COUNT'][1] + 5  # add the 5 people that finished high-school
        education_df.at[4, 'COUNT'] = education_df['COUNT'][4] + 1  # add 1 PhD
        education_df.at[3, 'COUNT'] = education_df['COUNT'][3] + 1  # add 1 Diplom

        print('Corrected distribution')
        print('QUESTION: ', variables_df.loc[variables_df['VAR'] == education]['QUESTION'].values[0])
        print('ANSWERS:')
        print(education_df.filter(items=['MEANING', 'COUNT']))
        print('__________________________________________________')

        plt.subplots(figsize=(3, 4))
        plt.bar(education_df['MEANING'], education_df['COUNT'])
        plt.ylabel('Amounts')
        plt.xticks(rotation=30, ha='right')
        plt.grid(axis='y')
        plt.tight_layout()
        plt.savefig(os.path.join(results_path, 'bar_degree.pdf'))

        """how many of each expert field ?"""
        expert_answers = ['BI02_01', 'BI02_02', 'BI02_03', 'BI02_09', 'BI02_07', 'BI02_04']  # , 'BI02_04a']

        experts_df = pd.DataFrame(columns=['FIELD', 'COUNT'])
        for answer in expert_answers:
            # get name of the id
            answer_name = variables_df.loc[variables_df['VAR'] == answer]['LABEL'].values[0]
            filtered_answer_name = str(answer_name).replace('Field of Expertise: ', '')

            # one means not checked, two means checked
            postive_answer_count = filtered_data_df[answer].value_counts()[2]

            new_row = {'FIELD': filtered_answer_name, 'COUNT': postive_answer_count}
            experts_df = experts_df.append(new_row, ignore_index=True)

        print('QUESTION: ', variables_df.loc[variables_df['VAR'] == 'BI02']['QUESTION'].values[0])
        print('ANSWERS:')
        print(experts_df)

        """other expertise answers"""
        other_expertise = filtered_data_df['BI02_04a'].dropna()
        print('"Other Field of Expertise"', other_expertise.shape[0], 'Answers:')
        print(other_expertise)
        print('__________________________________________________')

        plt.subplots(figsize=(4, 4))
        plt.bar(experts_df['FIELD'], experts_df['COUNT'])
        plt.ylabel('Amounts')
        plt.xticks(rotation=30, ha='right')
        plt.grid(axis='y')
        plt.tight_layout()
        plt.savefig(os.path.join(results_path, 'bar_expertise.pdf'))

        sys.stdout = original_stdout  # makes print write to console again

    """cloud comparisons"""
    comp = ['CC01', 'CC02', 'CC03', 'CC04', 'CC05', 'CC06', 'CC07', 'CC08', 'CC09', 'CC10', 'CC11', 'CC12', 'CC13',
            'CC14', 'CC15', 'CC16', 'CC17', 'CC18', 'CC19', 'CC20', 'CC21', 'CC22', 'CC23', 'CC24', 'CC25', 'CC26',
            'CC27', 'CC28', 'CC29', 'CC30', 'CC31', 'CC32']

    # sex = 'BI04'
    # questionaires_woman = filtered_data_df[filtered_data_df[sex] == 1]
    # questionaires_man = filtered_data_df[filtered_data_df[sex] == 2]
    #
    # comp_df_woman = questionaires_woman.filter(items=comp).copy()
    # comp_df_woman_counts = comp_df_woman.apply(pd.Series.value_counts)
    #
    # comp_df_man = questionaires_man.filter(items=comp).copy()
    # comp_df_man_counts = comp_df_man.apply(pd.Series.value_counts)

    """stacked bar chart"""

    comp_df = filtered_data_df.filter(items=comp).copy()
    # comp_counts = comp_df.apply(pd.Series.value_counts)
    comp_precentile_df = comp_df.apply(pd.Series.value_counts, args=(True,))

    # switch columns and rows
    comp_precentile_df = comp_precentile_df.T
    # reverse order
    comp_precentile_df = comp_precentile_df.iloc[::-1]
    # reorder columns
    comp_precentile_df = comp_precentile_df[[1, 3, 2]]
    # rename columns
    # comp_precentile_df = comp_precentile_df.rename(columns={1: 'Left Video', 2: 'Right Video', 3: 'I can\'t decide'})
    comp_precentile_df = comp_precentile_df.rename(columns={1: 'See Y-Label', 2: 'Cloud 7', 3: 'I can\'t decide'})

    # change labels to internal titles
    # comp_var_df = variables_df[variables_df['VAR'].isin(comp)]  # select only cloud compare questions
    # comp_var_df = comp_var_df.filter(['VAR', 'LABEL'])  # select only two columns needed
    # comp_var_df = comp_var_df.set_index('VAR')  # make var to index for merge
    # merged_df = pd.merge(comp_precentile_df, comp_var_df, left_index=True, right_index=True)  # merge percentile with names
    # comp_precentile_df = merged_df.set_index('LABEL')  # make label the new index

    # change labels to better titles
    better_names_path = os.path.join(results_path, 'Better_Names.csv')
    better_names_df = pd.read_csv(better_names_path, encoding='ANSI')
    better_names_df = better_names_df.filter(['VAR', 'LABEL_C_S'])  # select only two columns needed
    better_names_df = better_names_df.set_index('VAR')  # make var to index for merge
    comp_precentile_df = pd.merge(comp_precentile_df, better_names_df, left_index=True,
                                  right_index=True)  # merge percentile with names
    # comp_precentile_df = comp_precentile_df.set_index('LABEL')  # make label the new index

    # select all component comparisons
    comp_component = ['CC31', 'CC32', 'CC14', 'CC13', 'CC12', 'CC11', 'CC10', 'CC09', 'CC08', 'CC06', 'CC07', 'CC05',
                      'CC04', 'CC03', 'CC02', 'CC01']
    comp_component_df = comp_precentile_df.loc[comp_component].copy()
    comp_component_df = comp_component_df.set_index('LABEL_C_S')
    del comp_component_df.index.name
    plot_stacked_bar(comp_component_df, os.path.join(results_path, 'bar_comp_component.pdf'))

    # select all competition comparisons cloud 7
    comp_competition = ['CC22', 'CC21', 'CC20', 'CC19', 'CC18', 'CC17', 'CC16', 'CC15']
    comp_competition_df = comp_precentile_df.loc[comp_competition].copy()
    comp_competition_df = comp_competition_df.set_index('LABEL_C_S')
    del comp_competition_df.index.name
    plot_stacked_bar(comp_competition_df, os.path.join(results_path, 'bar_comp_competition.pdf'))

    # select all competition comparisons cloud 7 without billowmask
    comp_competition_bm = ['CC30', 'CC29', 'CC28', 'CC27', 'CC26', 'CC25', 'CC24', 'CC23']
    comp_competition_bm_df = comp_precentile_df.loc[comp_competition_bm].copy()
    comp_competition_bm_df = comp_competition_bm_df.set_index('LABEL_C_S')
    del comp_competition_bm_df.index.name
    # rename cloud 7 to Cloud 7 without billow mask
    comp_competition_bm_df = comp_competition_bm_df.rename(columns={'Cloud 7': 'Cloud 7 without billow mask'})
    plot_stacked_bar(comp_competition_bm_df, os.path.join(results_path, 'bar_comp_competition_bm.pdf'),
                     legendfontsize=8.5)

    """cloud error feedback"""
    error_close = ['CE01x01', 'CE01x02', 'CE01x03', 'CE01x04', 'CE01x05']
    error_close_df = filtered_data_df.filter(items=error_close).dropna(how='all').copy()
    error_close_df.to_csv(os.path.join(results_path, 'error_close.csv'), index=True)

    error_far = ['CE02x01', 'CE02x02', 'CE02x03', 'CE02x04', 'CE02x05']
    error_far_df = filtered_data_df.filter(items=error_far).dropna(how='all').copy()
    error_far_df.to_csv(os.path.join(results_path, 'error_far.csv'), index=True)


def plot_stacked_bar(df, output=None, legendfontsize=None):
    color = ['#4285f4', '#f4b400', '#db4437']  # colors
    ax = df.plot.barh(stacked=True, figsize=(8, (len(df.index) + 2.25) * 0.4), color=color)

    # add lables in the bars
    for i in ax.patches:
        w, h = i.get_width(), i.get_height()
        ax.text(i.get_x() + w / 2, i.get_y() + h / 2,
                "{:.0%}".format(w), ha="center", va="center")

    # legend
    if legendfontsize is not None:
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.,
                   fontsize=legendfontsize)
    else:
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=3, mode="expand", borderaxespad=0.)

    # x labels
    plt.xlim([0, 1])
    vals = ax.get_xticks()
    ax.set_xticklabels(['{:,.0%}'.format(x) for x in vals])

    plt.grid(axis='x')
    plt.tight_layout()

    if output is not None:
        plt.savefig(os.path.join(output))


def error_gen(actual, rounded):
    """
    https://stackoverflow.com/questions/13483430/how-to-make-rounded-percentages-add-up-to-100
    :param actual:
    :param rounded:
    :return:
    """
    divisor = math.sqrt(1.0 if actual < 1.0 else actual)
    return abs(rounded - actual) ** 2 / divisor


def round_to_100(percents):
    """
    https://stackoverflow.com/questions/13483430/how-to-make-rounded-percentages-add-up-to-100
    :param percents:
    :return:
    """
    if not math.isclose(sum(percents), 100):
        raise ValueError
    n = len(percents)
    rounded = [int(x) for x in percents]
    up_count = 100 - sum(rounded)
    errors = [(error_gen(percents[i], rounded[i] + 1) - error_gen(percents[i], rounded[i]), i) for i in range(n)]
    rank = sorted(errors)
    for i in range(up_count):
        rounded[rank[i][1]] += 1
    return rounded


def list_files_with_ext(directory, ext):
    """
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    """
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist


def multi_ztest(data_path, results_path, x1, x2_list):
    ztest_df = pd.DataFrame(columns=['X1', 'X2',
                                     'ztest_score', 'p_value_two-sided', 'p_value_larger', 'p_value_smaller'])
    x1_data = custom_data_picker(data_path, results_path, [x1])[0]
    x2_data_list = custom_data_picker(data_path, results_path, x2_list)

    for i in range(len(x2_list)):
        ztest_score_twosided, p_value_twosided = ztest(x1_data, x2_data_list[i], alternative='two-sided')
        ztest_score_larger, p_value_larger = ztest(x1_data, x2_data_list[i], alternative='larger')
        ztest_score_smaller, p_value_smaller = ztest(x1_data, x2_data_list[i], alternative='smaller')
        ztest_df.loc[len(ztest_df.index)] = [x1, x2_list[i],
                                             ztest_score_twosided, p_value_twosided, p_value_larger, p_value_smaller]
    return ztest_df


def get_pef_data(data_path):
    results_path = os.path.join('Profiling', 'FPSChartStats')

    # """Z-Test"""
    # # do not use null hypothesis significance testing with p-values for perf tests
    # # https://aakinshin.net/posts/statistics-for-performance/
    # x1 = 'comb'
    # x2_folders = ['comb_all_snapshots',
    #               'comb-adaptiveSampleCount',
    #               'comb-rain',
    #               'comb-wind',
    #               'comb-billow',
    #               'comb-billowmask',
    #               'comb-powder',
    #               'default',
    #               'harry_emelianov',
    #               'truesky',
    #               'empty']
    # ztest_df = multi_ztest(data_path, results_path, x1, x2_folders)
    #
    # x1_copy = 'comb-copy'
    # x2_folders_copy = ['comb-ani', 'comb_sim']
    # ztest_df_copy = multi_ztest(data_path, results_path, x1_copy, x2_folders_copy)
    # #
    # ztest_all = pd.concat([ztest_df, ztest_df_copy], ignore_index=True)
    # ztest_all.to_csv(os.path.join(data_path, 'ztest_all.csv'))

    """Lineplot copy vs ani"""

    comp_data_path = os.path.join(data_path, 'comb-copy', results_path)
    current_folder = os.path.join(comp_data_path, os.listdir(comp_data_path)[0])
    csv_path = list_files_with_ext(current_folder, 'csv')[0]  # only one csv in each folder
    df_comp_camera_1 = pd.read_csv(csv_path, skiprows=[0, 1, 2, 3])  # skipping rows cause first lines are quartiles

    comp_data_path = os.path.join(data_path, 'comb-ani', results_path)
    current_folder = os.path.join(comp_data_path, os.listdir(comp_data_path)[0])
    csv_path = list_files_with_ext(current_folder, 'csv')[0]  # only one csv in each folder
    df_ani_camera_1 = pd.read_csv(csv_path, skiprows=[0, 1, 2, 3])  # skipping rows cause first lines are quartiles

    plt.subplots(figsize=(9, 4))
    plt.plot('Time (sec)', 'Frame (ms)', data=df_comp_camera_1, label="Cloud 7 copy", color='#db4437')
    plt.plot('Time (sec)', 'Frame (ms)', data=df_ani_camera_1, label="Cloud 7 without FB animation", color='#4285f4')

    # format
    plt.xticks(rotation=30, ha='right')
    plt.ylabel('Frame (ms)')
    plt.xlabel('Time (sec)')
    plt.grid(axis='both')
    # plt.xlim([0, df_comp_camera_1['Time (sec)'].iloc[-1]])
    plt.xlim([0, 2])
    plt.ylim([9, 11.2])

    # line fir 90 fps but in ms
    max_fps_line = (1 / 90) * 1000
    plt.axhline(y=max_fps_line, color='#f4b400', linestyle='-', label='11ms')

    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(data_path, 'line_comb_copy_ani.pdf'))

    """comb all camera as separet box plots"""

    comp_camera_data = custom_camera_data_picker(data_path, results_path, 'comb')
    camera_names = ['Diagonal Sky',
                    'Bird\'s-eye view',
                    'Cloud Edge',
                    'Diagonal Edge',
                    'Center',
                    'Eye Level Center',
                    'Eye Level Edge',
                    'Eye Level Horizon',
                    'Ground']

    experiments_folders = [name for name in os.listdir(data_path) if os.path.isdir(os.path.join(data_path, name))]

    describe_all_df = pd.DataFrame()
    for folder in experiments_folders:
        camera_data = custom_camera_data_picker(data_path, results_path, folder)

        if folder == 'comb' or folder == 'comb_all_snapshots' or folder == 'comb-copy':
            colors = ['#db4437']
        else:
            colors = ['#4285f4']

        custom_boxplot(camera_data, camera_names, colors=colors,
                       output=os.path.join(data_path, 'boxplot_' + folder + '_camera.pdf'))

        # save all camera describtions in csv
        camera_df = pd.DataFrame()
        for i in range(len(camera_data)):
            camera_describe = pd.Series(camera_data[i].tolist()).describe(include='all').T
            name = pd.Series([camera_names[i]], index=['camera'])
            camera_describe = camera_describe.append(name)
            camera_df = camera_df.append(camera_describe, ignore_index=True)
        camera_df = camera_df.set_index('camera')
        camera_df.to_csv(os.path.join(data_path, folder + '_camera.csv'))

        # save all for all join describtions in csv
        joined_camera_data = pd.Series(np.concatenate(camera_data).tolist()).describe(
            percentiles=[.01, .05, .25, .50, .75, .95, .99], include='all').T
        name = pd.Series([folder], index=['folder'])
        joined_camera_data = joined_camera_data.append(name)
        describe_all_df = describe_all_df.append(joined_camera_data, ignore_index=True)

    describe_all_df = describe_all_df.set_index('folder')
    describe_all_df.to_csv(os.path.join(data_path, 'joined_perf_all_folders.csv'))

    """boxplot of perf experiments"""

    comp_effects_folders = ['comb',
                            'comb-rain',
                            'comb-wind',
                            'comb-billow',
                            'comb-billowmask',
                            'comb-powder']
    comp_effects_names = ['Cloud 7',
                          'Cloud 7 without rain',
                          'Cloud 7 without wind',
                          'Cloud 7 without billow',
                          'Cloud 7 without billow mask',
                          'Cloud 7 without powder']
    comp_effects_colors = ['#db4437', '#4285f4', '#4285f4', '#4285f4', '#4285f4', '#4285f4']
    comp_effects_data = custom_data_picker(data_path, results_path, comp_effects_folders)
    custom_boxplot(comp_effects_data, comp_effects_names, colors=comp_effects_colors,
                   output=os.path.join(data_path, 'boxplot_comb_vs_effects.pdf'))

    comp_animation_folders = ['comb_all_snapshots', 'comb']
    comp_ani_names = ['Cloud 7 with all snapshots', 'Cloud 7']
    comp_ani_colors = ['#db4437', '#db4437']
    comp_ani_data = custom_data_picker(data_path, results_path, comp_animation_folders)
    custom_boxplot(comp_ani_data, comp_ani_names, colors=comp_ani_colors, figsize=(3.5, 6),
                   output=os.path.join(data_path, 'boxplot_comb_vs_comb_all.pdf'))
    # comp_animation_folders = ['comb_all_snapshots']
    # comp_ani_names = ['Cloud 7 with all snapshots']
    # comp_ani_colors = ['#db4437']
    # single_custom_boxplot(comp_ani_data, comp_ani_names, colors=comp_ani_colors, figsize=(3, 4),
    #                output=os.path.join(data_path, 'boxplot_comb_vs_comb_all.pdf'))

    comp_animation_folders = ['comb-copy', 'comb-ani', 'comb_sim']
    comp_ani_names = ['Cloud 7 copy', 'Cloud 7 without FB animation',
                      'Cloud 7 without added features']
    comp_ani_colors = ['#db4437', '#4285f4', '#4285f4']
    comp_ani_data = custom_data_picker(data_path, results_path, comp_animation_folders)
    custom_boxplot(comp_ani_data, comp_ani_names, colors=comp_ani_colors,
                   output=os.path.join(data_path, 'boxplot_comb_vs_animation.pdf'))

    competition_folders = ['comb', 'default', 'harry_emelianov', 'truesky', 'empty', ]
    competition_names = ['Cloud 7', 'Default cloud', 'Emelianov clouds', 'trueSky', 'No clouds']
    competition_colors = ['#db4437', '#4285f4', '#4285f4', '#4285f4', '#4285f4']
    competition_data = custom_data_picker(data_path, results_path, competition_folders)
    custom_boxplot(competition_data, competition_names, colors=competition_colors,
                   output=os.path.join(data_path, 'boxplot_comb_vs_competition.pdf'))

    """adaptive sample count tests"""
    comp_wierd_folders = ['comb', 'comb-adaptiveSampleCount']
    comp_wierd_names = ['Cloud 7', 'Cloud 7 without ASC']
    comp_wierd_colors = ['#db4437', '#4285f4']
    comp_wierd_data = custom_data_picker(data_path, results_path, comp_wierd_folders)
    custom_boxplot(comp_wierd_data, comp_wierd_names, colors=comp_wierd_colors, step=2, figsize=(3.5, 5),
                   output=os.path.join(data_path, 'boxplot_comb_vs_wierd.pdf'))
    custom_boxplot(comp_wierd_data, comp_wierd_names, colors=comp_wierd_colors, step=1, figsize=(3.5, 5),
                   output=os.path.join(data_path, 'boxplot_comb_vs_wierd_cut.pdf'), ylim=[1, 17])

    # make table of only cameras where the sample count is different
    comb_camera_data = custom_camera_data_picker(data_path, results_path, 'comb')
    comb_asc_camera_data = custom_camera_data_picker(data_path, results_path, 'comb-adaptiveSampleCount')
    comb_camera_data_la = np.concatenate(comb_camera_data[3:8])
    comb_asc_camera_data_la = np.concatenate(comb_asc_camera_data[3:8])
    joined_data = np.array([comb_camera_data[0], comb_asc_camera_data[0],
                            comb_camera_data[1], comb_asc_camera_data[1],
                            comb_camera_data[2], comb_asc_camera_data[2],
                            comb_camera_data_la, comb_asc_camera_data_la], dtype=object)

    joined_colors = ['#db4437', '#4285f4', '#db4437', '#4285f4', '#db4437', '#4285f4', '#db4437', '#4285f4']
    joined_camera_names = ['Diagonal Sky', 'Diagonal Sky',
                           'Bird\'s-eye view', 'Bird\'s-eye view',
                           'Cloud Edge', 'Cloud Edge',
                           'Camera 4-9', 'Camera 4-9']
    joined_legend = [Patch(facecolor='#db4437', edgecolor='black', label='Cloud 7'),
                     Patch(facecolor='#4285f4', edgecolor='black', label='Cloud 7 without ASC'),
                     Line2D([0], [0], color='#f4b400', label='11ms')]
    custom_boxplot(joined_data, joined_camera_names, colors=joined_colors, ylim=[4, 27], step=1,
                   legend_elements=joined_legend,
                   output=os.path.join(data_path, 'boxplot_comb_vs_wierd_camera.pdf'))

    """historgram for all snapshots"""
    # comp_all_folders = ['comb_all_snapshots']
    # comp_all_data = custom_data_picker(data_path, results_path, comp_all_folders)[0]
    # # comp_all_data = custom_camera_data_picker(data_path, results_path, comp_all_folders[0])[0]
    #
    # plt.subplots(figsize=(4, 4))
    # plt.hist(comp_all_data, bins=100)
    # plt.grid(axis='y')
    # plt.tight_layout()
    # plt.show()


def custom_camera_data_picker(data_path, Results_path, folder):
    comp_camera_data = []
    comp_data_path = os.path.join(data_path, folder, Results_path)
    for folder in os.listdir(comp_data_path):
        current_folder = os.path.join(comp_data_path, folder)
        csv_path = list_files_with_ext(current_folder, 'csv')[0]  # only one csv in each folder
        dataframe = pd.read_csv(csv_path, skiprows=[0, 1, 2, 3])  # skipping rows cause first lines are quartiles
        comp_camera_data.append(dataframe['Frame (ms)'].values)
    comp_camera_data = np.array(comp_camera_data, dtype=object)
    return comp_camera_data


def custom_data_picker(data_path, Results_path, folder_list):
    folder_data = []
    for folder in folder_list:

        subfolder_data = np.array([], dtype=object)
        comp_data_path = os.path.join(data_path, folder, Results_path)
        for subfolder in os.listdir(comp_data_path):
            current_folder = os.path.join(comp_data_path, subfolder)
            csv_path = list_files_with_ext(current_folder, 'csv')[0]  # only one csv in each folder
            dataframe = pd.read_csv(csv_path,
                                    skiprows=[0, 1, 2, 3])  # skipping rows cause first lines are quartiles
            subfolder_data = np.append(subfolder_data,
                                       dataframe['Frame (ms)'].values)  # merge all data into one array

        folder_data.append(subfolder_data)  # subfolder array array
    folder_data = np.array(folder_data, dtype=object)

    return folder_data


def custom_boxplot(data, labels, colors=['#4285f4'], step=1.0, figsize=(8, 5), output=None, ylim=None,
                   legend_elements=None):
    plt.subplots(figsize=figsize)
    flierprops = dict(alpha=.33)
    # meanprops = dict(marker="x", markeredgecolor='w')
    bp = plt.boxplot(data, patch_artist=True, labels=labels, flierprops=flierprops)
    # , showmeans=False, meanprops=meanprops)

    # color them all rainbowpattern
    # cmap = cm.get_cmap('viridis')
    # number_of_boxes = len(comp_camera_data)
    # for i in range(number_of_boxes):
    #     current_color = cmap(i/(number_of_boxes-1))
    #     bp['boxes'][i].set_facecolor(current_color)
    #     plt.gca().get_xticklabels()[i].set_color(current_color)

    number_of_boxes = len(data)
    for i in range(number_of_boxes):
        if len(colors) == 1:
            current_color = colors[0]
        else:
            current_color = colors[i]
        bp['boxes'][i].set_facecolor(current_color)
    plt.setp(bp['medians'], color='black')

    # format
    plt.xticks(rotation=30, ha='right')
    # set y label interval
    minx = int(min(min(x) for x in data))
    maxx = int(max(max(x) for x in data)) + (2 * step)
    plt.yticks(np.arange(minx, maxx, step))
    plt.ylabel('Frame (ms)')
    plt.grid(axis='y')

    if ylim is not None:
        plt.ylim(ylim)

    # line fir 90 fps but in ms
    max_fps_line = (1 / 90) * 1000
    plt.axhline(y=max_fps_line, color='#f4b400', linestyle='-', label='11ms')
    if legend_elements is not None:
        plt.legend(handles=legend_elements, loc='upper left')
    else:
        plt.legend()
    plt.tight_layout()

    if output is not None:
        plt.savefig(output)
    plt.close('all')


def single_custom_boxplot(data, labels, colors=['#4285f4'], step=1.0, figsize=(8, 5), output=None, ylim=None,
                          legend_elements=None):
    plt.subplots(figsize=figsize)
    flierprops = dict(alpha=.33)
    # meanprops = dict(marker="x", markeredgecolor='w')
    bp = plt.boxplot(data, patch_artist=True, labels=labels, flierprops=flierprops)
    # , showmeans=False, meanprops=meanprops)

    # color them all rainbowpattern
    # cmap = cm.get_cmap('viridis')
    # number_of_boxes = len(comp_camera_data)
    # for i in range(number_of_boxes):
    #     current_color = cmap(i/(number_of_boxes-1))
    #     bp['boxes'][i].set_facecolor(current_color)
    #     plt.gca().get_xticklabels()[i].set_color(current_color)

    bp['boxes'][0].set_facecolor(colors[0])
    plt.setp(bp['medians'], color='black')

    # set y label interval
    minx = int(min(data))
    maxx = int(max(data)) + (2 * step)
    plt.yticks(np.arange(minx, maxx, step))
    plt.ylabel('Frame (ms)')
    plt.grid(axis='y')

    if ylim is not None:
        plt.ylim(ylim)

    # line fir 90 fps but in ms
    max_fps_line = (1 / 90) * 1000
    plt.axhline(y=max_fps_line, color='#f4b400', linestyle='-', label='11ms')
    if legend_elements is not None:
        plt.legend(handles=legend_elements, loc='upper left')
    else:
        plt.legend()
    plt.tight_layout()

    if output is not None:
        plt.savefig(output)
    plt.close('all')


def gridspacing_plot(linear=True, output=None):
    fig, ax = plt.subplots(figsize=(4, 4))

    # add rectangle to plot
    # ax.add_patch(Rectangle((0, 0), 1.5, 1.5))

    min_extent, max_extent = 0, 6
    if linear:
        # linear appearing spacing
        spacing = np.split(np.arange(0, max_extent * max_extent, 1), max_extent)
        # for i in spacing:
        #     print(i)
    else:
        # nonlinear appearing spacing
        spacing = [[0, 0, 0, 1, 1, 2, 3, 4, 4, 5, 5, 5],
                   [0, 0, 0, 1, 1, 2, 3, 4, 4, 5, 5, 5],
                   [0, 0, 0, 1, 1, 2, 3, 4, 4, 5, 5, 5],
                   [6, 6, 6, 7, 7, 8, 9, 10, 10, 11, 11, 11],
                   [6, 6, 6, 7, 7, 8, 9, 10, 10, 11, 11, 11],
                   [12, 12, 12, 13, 13, 14, 15, 16, 16, 17, 17, 17],
                   [18, 18, 18, 19, 19, 20, 21, 22, 22, 23, 23, 23],
                   [24, 24, 24, 25, 25, 26, 27, 28, 28, 29, 29, 29],
                   [24, 24, 24, 25, 25, 26, 27, 28, 28, 29, 29, 29],
                   [30, 30, 30, 31, 31, 32, 33, 34, 34, 35, 35, 35],
                   [30, 30, 30, 31, 31, 32, 33, 34, 34, 35, 35, 35],
                   [30, 30, 30, 31, 31, 32, 33, 34, 34, 35, 35, 35]]

    ax.imshow(spacing, extent=(min_extent, max_extent, min_extent, max_extent), aspect='equal', interpolation='none',
              cmap='viridis')

    # set number of axis labels
    ticks = np.arange(min_extent, max_extent + 1, 1)
    plt.xticks(ticks)
    plt.yticks(ticks)

    # restrict view
    plt.xlim([min_extent, max_extent])
    plt.ylim([min_extent, max_extent])

    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(16)

    fig.tight_layout()

    if output is not None:
        plt.savefig(output)


def array_sphere(shape, radius, position):
    # https://stackoverflow.com/questions/46626267/how-to-generate-a-sphere-in-3d-numpy-array
    # assume shape and position are both a 3-tuple of int or float
    # the units are pixels / voxels (px for short)
    # radius is a int or float in px
    semisizes = (radius,) * 3

    # genereate the grid for the support points
    # centered at the position indicated by position
    grid = [slice(-x0, dim - x0) for x0, dim in zip(position, shape)]
    position = np.ogrid[grid]
    # calculate the distance of all points from `position` center
    # scaled by the radius
    arr = np.zeros(shape, dtype=float)
    for x_i, semisize in zip(position, semisizes):
        # this can be generalized for exponent != 2
        # in which case `(x_i / semisize)`
        # would become `np.abs(x_i / semisize)`
        arr += (x_i / semisize) ** 2

    # the inner part of the sphere will have distance below 1
    return (arr <= 1.0).astype(int)


def custom_imshow(data, output=None, figsize=(4, 4), fontsize=24):
    xmin_extent, xmax_extent = 0, len(data[1])
    ymin_extent, ymax_extent = 0, len(data)
    fig, ax = plt.subplots(figsize=figsize)
    ax.imshow(data, extent=(xmin_extent, xmax_extent, ymin_extent, ymax_extent), interpolation='none', aspect='equal'
              , cmap='viridis')

    # set number of axis labels
    plt.xticks(np.arange(xmin_extent, xmax_extent + 1, 1))
    plt.yticks(np.arange(ymin_extent, ymax_extent + 1, 1))

    # line fir 90 fps but in ms
    # max_fps_line = 2
    # plt.axvline(x=max_fps_line, color='black', linestyle='-', label='11ms')

    # restrict view
    # plt.xlim([min_extent, max_extent])
    # plt.ylim([min_extent, max_extent])
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(fontsize)

    fig.tight_layout()

    if output is not None:
        plt.savefig(output)


def volume_texture_plot(size=2, figsize=(3, 3), fontsize=12, output=None):
    # size, radius, position = 4, 2, 1.5
    # sphere = array_sphere((size, size, size), radius, (position, position, position))
    sphere = np.array(range(size ** 3))
    sphere = sphere.reshape(size, size, size)

    norm = plt.Normalize()
    sphere_color = plt.cm.viridis(norm(sphere))

    """2D plots """
    # for i in range(len(sphere_color)):
    #     custom_imshow(sphere_color[i], output=os.path.join(latex_path, '2D_volume_texture_' + str(i) + '.jpg'),
    #                   figsize=figsize, fontsize=fontsize)

    # stacked_sphere_color = np.concatenate((sphere_color[1], sphere_color[0]), axis=1)
    # stacked_sphere_color = np.flip(stacked_sphere_color, 0) # flip to align with coordinate system of the others
    stacked_sphere_color = np.flip(sphere, (0, 1))
    stacked_sphere_color, x = nc_translator.tile_array(stacked_sphere_color)
    if output is not None:
        custom_imshow(stacked_sphere_color, output=os.path.join(output, '2D_volume_texture.png'),
                      figsize=figsize, fontsize=fontsize)

    """3D plots stacked"""
    # positions of surface
    x = np.arange(0, size + 1, 1)
    y = np.arange(0, size + 1, 1)
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)

    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, projection='3d', proj_type='ortho')

    # Plot a 3D surface
    for i in range(len(sphere_color)):
        ax.plot_surface(X, Y, Z + i, facecolors=sphere_color[i])

    # set number of axis labels
    min_extent, max_extent = 0, len(sphere)
    ticks = np.arange(min_extent, max_extent + 1, 1)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_zticks(ticks)
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels() + ax.get_zticklabels()):
        item.set_fontsize(fontsize)

    fig.tight_layout()
    if output is not None:
        plt.savefig(os.path.join(output, '2D-3D_volume_texture.png'))

    """3D Voxel plot"""
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, projection='3d', proj_type='ortho')

    sphere_color = np.flip(np.rot90(sphere_color, axes=(0, 2)), 0)  # rotate flip cause voxels are oriented differently
    ax.voxels(np.ones_like(sphere), facecolors=sphere_color)

    # set number of axis labels
    min_extent, max_extent = 0, len(sphere)
    ticks = np.arange(min_extent, max_extent + 1, 1)
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_zticks(ticks)
    # ax.set_xlabel('x')
    # ax.set_ylabel('y')
    # ax.set_zlabel('z')
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels() + ax.get_zticklabels()):
        item.set_fontsize(fontsize)

    fig.tight_layout()
    if output is not None:
        plt.savefig(os.path.join(output, '3D_volume_texture.png'))


def channelstacking(pngdir):
    png_path = os.path.join(pngdir, 'channel_red.png')
    channel_red = imageio.imread(png_path)

    png_path = os.path.join(pngdir, 'channel_green.png')
    channel_green = imageio.imread(png_path)

    png_path = os.path.join(pngdir, 'channel_blue.png')
    channel_blue = imageio.imread(png_path)

    png_path = os.path.join(pngdir, 'channel_apha.png')
    channel_apha = imageio.imread(png_path)

    channel_stacked = [channel_red, channel_green, channel_blue, channel_apha]
    nc_translator.save_png(channel_stacked, os.path.join(pngdir, 'channel_stacked.png'))


def absulteerrorplot(image1path, image2path, outputpath, vmin=None, vmax=None):
    # load and turn with L into grayscale
    img1 = np.array(Image.open(image1path).convert('L'))
    img2 = np.array(Image.open(image2path).convert('L'))

    # np.uint8 or np.uint32 doesnt work because it doesnt have negative numbers
    img1 = img1.astype('int32')
    img2 = img2.astype('int32')
    # subtract one from the other and then flip all values above 0
    imgdiff = np.absolute(np.subtract(img1, img2))
    print('min and max difference:',imgdiff.min(), imgdiff.max())

    # change datatype to 0-255 numbers again if you want to store it as an image again
    # imgdiff = imgdiff.astype(np.uint8)
    # Image.fromarray(imgdiff).save(outputpath)

    # if you want a colormap applied use this
    yres, xres = imgdiff.shape  # this is to make sure the image stays the same rez
    plt.figure(figsize=(xres / 100, yres / 100), dpi=100, frameon=False)
    plt.axis('off')
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
    plt.imshow(imgdiff, interpolation='none', cmap='viridis', vmin=vmin, vmax=vmax)
    plt.savefig(outputpath, bbox_inches='tight', pad_inches=0)


def describeperfdata(data_path, individualstat=False):
    """
    creates csv files that describe the perfromance data from unreal engine.
    :param data_path:path containing folders that contain Profiling/FPSChartStats perfdata from ue4
    :return:
    """
    results_path = os.path.join('Profiling', 'FPSChartStats')
    camera_names = ['Diagonal Sky',
                    'Bird\'s-eye view',
                    'Cloud Edge',
                    'Diagonal Edge',
                    'Center',
                    'Eye Level Center',
                    'Eye Level Edge',
                    'Eye Level Horizon',
                    'Ground']

    experiments_folders = [name for name in os.listdir(data_path) if os.path.isdir(os.path.join(data_path, name))]
    output_path = os.path.join(data_path, 'joined_perf_all_folders.csv')

    if os.path.isfile(output_path):
        describe_all_df = pd.read_csv(output_path)
    else:
        describe_all_df = pd.DataFrame()

    for folder in experiments_folders:
        if not describe_all_df.empty:
            if folder in describe_all_df['folder'].values:
                continue

        camera_data = custom_camera_data_picker(data_path, results_path, folder)

        # save all camera describtions in csv
        camera_df = pd.DataFrame()
        for i in range(len(camera_data)):
            camera_describe = pd.Series(camera_data[i].tolist()).describe(include='all').T
            name = pd.Series([camera_names[i]], index=['camera'])
            camera_describe = camera_describe.append(name)
            camera_df = camera_df.append(camera_describe, ignore_index=True)
        camera_df = camera_df.set_index('camera')
        if individualstat:
            camera_df.to_csv(os.path.join(data_path, folder + '_camera.csv'), float_format='%.6g')

        # save all for all join describtions in csv
        joined_camera_data = pd.Series(np.concatenate(camera_data).tolist()).describe(include='all').T
        name = pd.Series([folder], index=['folder'])
        joined_camera_data = joined_camera_data.append(name)
        describe_all_df = describe_all_df.append(joined_camera_data, ignore_index=True)
        print('finished folder ', str(folder))

    describe_all_df = describe_all_df.set_index('folder')
    describe_all_df.to_csv(output_path, float_format='%.6g')


def main():
    # make graphs for the survey data
    survey_data_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'experiment_user',
                                    'Results')
    # get_survey_data(survey_data_path)

    # make graphs for the performance data
    perf_data_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'experiment_perf',
                                  'Results')
    # get_pef_data(perf_data_path)

    # make graphs for explaining stuff
    latex_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Latex')
    # gridspacing_plot(linear=False, output=os.path.join(latex_path, '6x6_nonlinear.png'))
    # gridspacing_plot(linear=True, output=os.path.join(latex_path, '6x6_linear.png'))
    # volume_texture_plot(size=2, figsize=(3, 3), fontsize=12, output=latex_path)
    # channelstacking(latex_path)

    # make absolute difference plots
    imagedir = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow')
    image1path = os.path.join(imagedir, 'scalarflow_blend.png')
    image2path = os.path.join(imagedir, 'scalarflow_ue4_8bit.png')
    image3path = os.path.join(imagedir, 'scalarflow_ue4_16bit.png')
    outputpath = os.path.join(imagedir, 'output.png')
    # absulteerrorplot(image2path, image1path, os.path.join(imagedir, 'scalarflow_ue4_16bit-blend_difference.png'))
    # absulteerrorplot(image2path, image3path, os.path.join(imagedir, 'scalarflow_ue4_8-16bit_difference.png')
    #                  , vmin=0, vmax=87)

    # make description csv to compare performance data
    perf_data_path = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master',
                                  'experiment_perf_development')
    describeperfdata(perf_data_path)

if __name__ == "__main__":
    # execute only if run as a script
    main()