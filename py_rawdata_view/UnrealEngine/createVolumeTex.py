import os
import argparse
import unreal


def list_files_with_ext(directory, ext):
    """
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    """
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist


def create_generic_asset(asset_path='', unique_name=None, asset_class=None, asset_factory=None):
    # https://www.youtube.com/watch?v=1eWt1AOdCg0 create asset ue python
    # asset_path: str : Path of asset to create
    # unique_name: bool : If True, will add a number at the end of the asset name until unique
    # asset_class: obj unreal.Class : The asset class
    # asset_factory: obj unreal.Factory : The associated factory of the class.
    # return: obj : The created asset

    if unique_name is None or unique_name == '':
        asset_path, asset_name = unreal.AssetToolsHelpers. \
            get_asset_tools().create_unique_asset_name(base_package_name=asset_path, suffix='')
    else:
        asset_name = unique_name

    if not unreal.EditorAssetLibrary.does_asset_exist(asset_path=asset_path):
        path = asset_path.rsplit('/', 1)[0]  # cause for some reason the name is also contained
        return unreal.AssetToolsHelpers.get_asset_tools(). \
            create_asset(asset_name=asset_name, package_path=path, asset_class=asset_class, factory=asset_factory)
    return unreal.load_asset(asset_path)


def get_selected_assets():
    """get the assets currently selected in the Content Browser"""
    utility_base = unreal.GlobalEditorUtilityBase.get_default_object()
    return utility_base.get_selected_assets()


def filter_type(listx, typex='Texture2D'):
    """
    returns only volume textures from the list of assets
    :param typex:
    :param listx: list of assets
    :return: list of volumetexture assets
    """

    new_list = []

    for item in listx:
        asset_class = item.get_class()
        class_name = unreal.SystemLibrary().get_class_display_name(asset_class)
        if class_name == typex:
            new_list.append(item)

    return new_list


def transfrom_selected(output_path, compression_setting=None, maximum_texture_size=None, mips=True, tilesize=512):
    """
    Translates all 2D Textures files in the Content Browser into volume textures with the proper settings
    :param tilesize: size of the tiles for the volume texture
    :param mips: bool of mips should be generated
    :param output_path: save location of the created volume textures. The 'Content' folder is called 'Game'
     The rest named as it seen in the explorer or content browser. The separator for folders is '/'.
    is in the content browser
    :param compression_setting: None if you want it to be default DST1 compression, 'GS' for grayscale amd 'Vector' for
    vector displacement. If you want to have the other compression settings, the names are in this link:
    https://docs.unrealengine.com/en-US/PythonAPI/class/TextureCompressionSettings.html?highlight=texturecompressionsettings#unreal.TextureCompressionSettings
    :param maximum_texture_size: None if you want to leave it as it. If you want to make the texture smaller just write the size
    of the tile you want to compress to.
    :return:
    """
    selected_assets = get_selected_assets()
    texture2D_list = filter_type(selected_assets)

    for texture2D in texture2D_list:
        texture_filename = str(texture2D.get_fname()) + '_v'
        volume_texture = create_generic_asset(output_path + 'VolumeTexture', texture_filename,
                                              unreal.VolumeTexture, unreal.VolumeTextureFactory())
        volume_texture_path = volume_texture.get_full_name()

        volume_texture.set_editor_property('source2d_texture', texture2D)

        # splits the name and takes everything after the x and everything before the _ after the x
        # tile_size_x = int(str(texture_filename).rsplit('x', 1)[1].split('_', 1)[0])
        # tile_size_y = int(str(texture_filename).rsplit('x', 1)[1].split('_', 1)[0])

        # https://docs.unrealengine.com/en-US/PythonAPI/class/TextureCompressionSettings.html?highlight=texturecompressionsettings#unreal.TextureCompressionSettings
        if compression_setting == 'GS':
            volume_texture.set_editor_property('compression_settings', unreal.TextureCompressionSettings.TC_GRAYSCALE)

        elif compression_setting == 'Vector':
            # texture2D.set_editor_property('compression_settings', unreal.TextureCompressionSettings.TC_VECTOR_DISPLACEMENTMAP)
            # texture2D.set_editor_property('srgb', False)
            # unreal.EditorAssetLibrary.save_asset(texture2D.get_full_name(), only_if_is_dirty=False)
            volume_texture.set_editor_property('compression_settings',
                                               unreal.TextureCompressionSettings.TC_VECTOR_DISPLACEMENTMAP)
        elif compression_setting == '16bit':
            volume_texture.set_editor_property('compression_settings', unreal.TextureCompressionSettings.TC_HDR)

        elif compression_setting == 'BC7':
            texture2D.set_editor_property('compression_settings', unreal.TextureCompressionSettings.TC_BC7)
            texture2D.set_editor_property('srgb', False)
            texture2D.set_editor_property('never_stream', False)
            texture2D.set_editor_property('virtual_texture_streaming', True)
            unreal.EditorAssetLibrary.save_asset(texture2D.get_full_name(), only_if_is_dirty=False)

            volume_texture.set_editor_property('compression_settings',
                                               unreal.TextureCompressionSettings.TC_BC7)

        volume_texture.set_editor_property('source2d_tile_size_x', tilesize)
        volume_texture.set_editor_property('source2d_tile_size_y', tilesize)
        volume_texture.set_editor_property('mip_gen_settings', unreal.TextureMipGenSettings.TMGS_FROM_TEXTURE_GROUP)
        volume_texture.set_editor_property('never_stream', False)
        volume_texture.set_editor_property('srgb', False)

        if not mips:
            volume_texture.set_editor_property('mip_load_options', unreal.TextureMipLoadOptions.ONLY_FIRST_MIP)
            volume_texture.set_editor_property('mip_gen_settings', unreal.TextureMipGenSettings.TMGS_NO_MIPMAPS)

        if maximum_texture_size is not None:
            volume_texture.set_editor_property('max_texture_size', maximum_texture_size)

        unreal.EditorAssetLibrary.save_asset(volume_texture_path, only_if_is_dirty=False)
        print(str(texture_filename), ' complete')


def edit_selected():
    selected_assets = get_selected_assets()

    for asset in selected_assets:
        # https://docs.unrealengine.com/5.0/en-US/PythonAPI/class/Texture.html?highlight=mip_load_options#unreal.Texture.mip_load_options
        # https://docs.unrealengine.com/5.0/en-US/PythonAPI/class/TextureMipLoadOptions.html#unreal.TextureMipLoadOptions
        asset.set_editor_property('mip_load_options', unreal.TextureMipLoadOptions.ONLY_FIRST_MIP)
        asset.set_editor_property('mip_gen_settings', unreal.TextureMipGenSettings.TMGS_NO_MIPMAPS)
        unreal.EditorAssetLibrary.save_asset(asset.get_full_name(), only_if_is_dirty=False)


def main():
    # 'Content' folder in editor is called 'Game'. just replace in path and it should work just fine

    edit_selected()

    """translate greyscale png to volume texture"""
    # output_path = '/Game/CustomContent/Textures/rawdata_view/qc_v/'
    # transfrom_selected(output_path, compression_setting='GS', maximum_texture_size=None)
    #
    # output_path = '/Game/CustomContent/Textures/rawdata_view_3k/qc_v/'
    # transfrom_selected(output_path, compression_setting='GS', maximum_texture_size=None)

    # output_path = '/Game/CustomContent/Textures/rawdata_view_6k/qc_v/'
    # transfrom_selected(output_path, compression_setting='GS', maximum_texture_size=None, tilesize=1024)

    """translate rgba png to volume texture"""
    # output_path = '/Game/CustomContent/Textures/rawdata_view/uinterp_vinterp_winterp_qr_v/'
    # # downscale for voltextures means the tile
    # transfrom_selected(output_path, compression_setting='Vector', maximum_texture_size=256)

    # output_path = '/Game/CustomContent/Textures/rawdata_view_6k/uinterp_vinterp_winterp_qr_v/'
    # # downscale for voltextures means the tile
    # transfrom_selected(output_path, compression_setting='Vector', maximum_texture_size=512, tilesize=1024)

    # output_path = '/Game/CustomContent/Textures/ScalarFlow/sim_070_density_16bit_v/'
    # # downscale for voltextures means the tile
    # transfrom_selected(output_path, compression_setting='16bit', maximum_texture_size=None)

    """translate rgb to volume texture"""
    # output_path = '/Game/CustomContent/Textures/rawdata_view/UVWinterp_v/'
    # transfrom_selected(output_path, compression_setting='BC7', maximum_texture_size=256, mips=True, tilesize=512)

    # output_path = '/Game/CustomContent/Textures/rawdata_view/qc_qr_v/'
    # transfrom_selected(output_path, compression_setting='BC7', maximum_texture_size=None, mips=True, tilesize=512)

    # output_path = '/Game/CustomContent/Textures/rawdata_view_6k/UVWinterp_v/'
    # transfrom_selected(output_path, compression_setting='BC7', maximum_texture_size=512, mips=True, tilesize=1024)

    # output_path = '/Game/CustomContent/Textures/rawdata_view_6k/qc_qr_v/'
    # transfrom_selected(output_path, compression_setting='BC7', maximum_texture_size=None, mips=True, tilesize=1024)

    """command to run the script in unreal"""
    # command to run this in the unreal editor in CMD, while the 2dtextures are marked in contentbrowser:
    # py "D:\BoringStuff\Uni-LMU-MMI\Master\cloud_thesis_4_27\py_rawdata_view\UnrealEngine\createVolumeTex.py"


if __name__ == "__main__":
    # execute only if run as a script
    main()
