import os
import numpy as np

node = hou.pwd()
geo = node.geometry()

print('reading csv')

filename = os.path.join('D:/', 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'cloud-thesis', 'rawdata_view', 'py_rawdata_view',
                        'cm1out_001030_qc_rescaled.csv')
xres, yres, zres = 1140, 1140, 40

numpy_array = np.genfromtxt(filename, skip_header=True)

volume_bbox = hou.BoundingBox(0, 0, 0, xres, yres, zres)
volume = geo.createVolume( xres, yres, zres, volume_bbox)
volume.setAllVoxels(numpy_array)

print('import done')
