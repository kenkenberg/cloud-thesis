# for Python 3.8
import sys  # to change print write to log and not console
import os  # read directories
import numpy as np  # nc imports and np
import math  # sqrt and such
import png  # to save as png
import pandas as pd
from scipy import interpolate
import matplotlib.pyplot as plt


def rescale_x_bit(data, bit=8, vmin=None, vmax=None):
    """
    takes any np array. and rescales it to use the full bit range of integer values.
    :param data: data array of numbers
    :param bit: bit range of 8, 16 or 32
    :param vmin: minimum value to be set the new 0, values that were smaller will also be 0
    :param vmax: maximum value, values that are larger will be capt at the maximum
    :return: returns np array with 16bit int of the data shape
    """

    # check if bit range is acceptable
    if bit != 8 and bit != 16:
        raise RuntimeError('only select bit range of 8, 16 or 32')

    # minus 1 because we start from 0
    bit_range = (2 ** bit) - 1

    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the int16 limit or under
        data = data.clip(min=vmin, max=vmax)

    if value_range == 0:  # to avoid division by 0
        rescaled_data = np.zeros(np.shape(data), dtype=data.dtype)  # everything will be set 0 as well
    else:
        rescaled_data = (bit_range * ((data - vmin) / value_range))

    # set datatype to the proper bit range
    if bit == 8:
        rescaled_data = rescaled_data.astype(np.uint8)
    else:
        rescaled_data = rescaled_data.astype(np.uint16)

    return rescaled_data


def tile_array(data, base2=True):
    """
    takes 3d array and tiles all the slices of the z dimension next to each other in a 2d array
    :param data: 3d data array of the shape (z, y, x)
    :param base2: if true the output array as well as its tiles will have a shape of a multiple of 2
    :return: 2d np array
    """

    # calculate size of 2d array
    (z_length, y_length, x_length) = np.shape(data)  # NOTE!!! the order of the Z,Y,X given by np.shape

    if base2:  # calculating size of base to image and its tiling
        new_x_length, new_y_length = 2, 2
        while x_length > new_x_length:  # image with base 2 to fit one tile in x length
            new_x_length = new_x_length * 2
        while y_length > new_y_length:  # image with base 2 to fit one tile in y length
            new_y_length = new_y_length * 2

        new_x_tiling_length, new_y_tiling_length = new_x_length, new_y_length
        z, x_offset, y_offset = 0, 0, 0
        while z < z_length:  # check if all tiles fit into image
            x_offset += new_x_tiling_length  # next column
            if x_offset > (new_x_length - new_x_tiling_length):  # new row
                if new_x_length <= new_y_length:
                    new_x_length += new_x_length  # expand image to the side, because we are out of space
                else:
                    new_y_length += new_y_length  # expand image below, because we are out of space
                    x_offset = 0  # back to column 0
                    y_offset += new_y_tiling_length
            z = math.ceil((new_x_length / new_x_tiling_length) * (new_y_length / new_y_tiling_length))  # update z
        new_data = np.zeros((new_y_length, new_x_length), dtype=data.dtype)
    else:
        number_of_rows = math.ceil(math.sqrt(z_length))
        new_x_tiling_length, new_y_tiling_length = x_length, y_length
        new_x_length, new_y_length = number_of_rows * x_length, number_of_rows * y_length
        new_data = np.zeros((new_y_length, new_x_length), dtype=data.dtype)

    # place the slices of the 3d array in the new empty 2d array
    x_offset, y_offset = 0, 0  # where we place the next image

    for z in range(z_length):
        # place array (x and y are flipped cause im dumb, but has to stay like this)
        new_data[y_offset:y_offset + y_length, x_offset:x_offset + x_length] = data[z]
        x_offset += new_x_tiling_length  # next column
        if x_offset > (new_x_length - new_x_tiling_length):
            x_offset = 0  # back to column 0
            y_offset += new_y_tiling_length  # new row

    number_of_tile = math.ceil(new_y_length / new_y_tiling_length) * math.ceil(new_x_length / new_x_tiling_length)
    # squared col_and_row_number because the other tiles will be empty but needed for proper scaling
    shape = (number_of_tile, new_x_tiling_length, new_y_tiling_length)

    return new_data, shape


def save_png(data, outputfile, bit=8):
    """
    saves a up to 4 2 dimensional arrays into a png
    :param data: array in the shape (1-4, X, Y) filled with integers from 0 - (2^bit)-1
        Note:   1 2D array will make a greyscale image
                2 2D arrays will make a grayscale image with alpha
                3 2D arrays will make a RGB image
                4 2D arrays will make a RGBA image
    :param outputfile: path of the png file
    :param bit: 8 or 16 for 8 or 16 bit per channel
    :return:
    """

    if data.__len__() > 4:
        raise RuntimeError('No more than 4 variables per PNG. PNGs have only four channels.')
    elif data.__len__() == 1:
        greyscale, alpha = True, False
        png_data = data[0]
    elif data.__len__() == 2:
        greyscale, alpha = True, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    elif data.__len__() == 3:
        greyscale, alpha = False, False
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])
    else:
        greyscale, alpha = False, True
        png_data = np.array([np.concatenate(row) for row in np.dstack(data)])

    print(np.shape(data[0]))
    # Use pypng to write z as a color PNG.
    with open(outputfile, 'wb') as f:
        writer = png.Writer(width=np.shape(data[0])[1], height=np.shape(data[0])[0],
                            bitdepth=bit, greyscale=greyscale, alpha=alpha)
        writer.write(f, png_data)


def make_uv(size, bit, output):
    """
    makes a generic UV texture
    :param size: resolution in XY
    :param bit: bit depth of texture
    :param output: output filepath
    :return:
    """
    line = np.arange(size)  # makes a single line
    uv_r = np.array([line for x in range(size)])  # repeat it for rows
    uv_g = np.swapaxes(uv_r, 0, 1)  # flip it for V coordinates
    uv_b = np.zeros((size, size), dtype=int)  # make 0 for blue channel

    data = np.array([uv_r, uv_g, uv_b])  # join them
    data = rescale_x_bit(data, bit=bit)  # rescale them to the color range of image
    save_png(data, output, bit=bit)  # save as image


def make_volumetric_uv(size, bit, output):
    """
    makes a volumetric UV texture
    :param size: resolution in XY
    :param bit: bit depth of texture
    :param output: output filepath
    :return:
    """
    line = np.arange(size)  # makes a single line
    uv_r = np.array([line for x in range(size)])  # repeat it for rows
    uv_r_vol = np.array([uv_r for x in range(size)])  # repeat it for height
    uv_g_vol = np.swapaxes(uv_r_vol, 1, 2)  # flip it for V coordinates
    uv_b_vol = np.array([np.full((size, size), x, dtype=int) for x in range(size)])  # make X

    uv_r_tiled, shape = tile_array(uv_r_vol)  # tile them to be 2D again
    uv_g_tiled, shape = tile_array(uv_g_vol)
    uv_b_tiled, shape = tile_array(uv_b_vol)

    data = np.array([uv_r_tiled, uv_g_tiled, uv_b_tiled])  # join them
    data = rescale_x_bit(data, bit=bit)  # rescale them to the color range of image
    save_png(data, output, bit=bit)  # save as image


def make_one_d_uv(size, bit, output):
    """
    makes one dimentions UV texture
    :param size: size in x dir
    :param bit: bit depth of texture
    :param output: output file direction
    :return:
    """
    uv_r = np.arange(size)  # makes a single line
    uv_g = uv_r
    uv_b = uv_r

    data = np.array([[uv_r], [uv_g], [uv_b]])  # join them
    data = rescale_x_bit(data, bit=bit)  # rescale them to the color range of image
    save_png(data, output, bit=bit)  # save as image


def main():
    size = 256
    bit = 8

    output_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                   'CustomTextures', 'UV_Generic.png')
    # make_uv(size, bit, output_filepath)

    output_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                   'CustomTextures', 'UV_Volumetric.png')
    # make_volumetric_uv(size, bit, output_filepath)

    output_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                   'CustomTextures', 'UV_1D.png')
    # make_one_d_uv(size, bit, output_filepath)


if __name__ == "__main__":
    # execute only if run as a script
    main()
