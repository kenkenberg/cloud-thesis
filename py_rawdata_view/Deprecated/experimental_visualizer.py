import os  # read directories
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.animation import FuncAnimation
import yt


def plot_3darray(array, logarithmicColor=False):
    """
    visualize a simple 3D array as an animated 2d plot
    :param array: simple 3D array
    :param logarithmicColor: if the colormap should be logarithmic so some datasets are at least visible
    :return:
    """

    # get stats from array
    shape = np.shape(array)  # canvas size and frame count
    vmin, vmax = np.amin(array), np.amax(array)  # min max will otherwise be taken from first frame

    fig, ax = plt.subplots()

    if logarithmicColor:
        image = plt.imshow(array[0], interpolation='none',
                           norm=colors.LogNorm(vmin=vmin, vmax=vmax))
    else:
        image = plt.imshow(array[0], interpolation='none', vmin=vmin, vmax=vmax)
    text = plt.title('Frame: 000')

    # animation function updating the image and pausing with click
    def animate_func(i):

        # click to pause animation
        anim_running = True

        def onClick(event):
            nonlocal anim_running
            if anim_running:
                anim.event_source.stop()
                anim_running = False
            else:
                anim.event_source.start()
                anim_running = True

        fig.canvas.mpl_connect('button_press_event', onClick)

        image.set_array(array[i])
        text.set_text('Frame: ' + str(i).zfill(3))
        return [image]

    anim = FuncAnimation(fig, animate_func, frames=shape[0], interval=45, repeat=True)
    anim.save('animation.gif', writer='MovieWriter', fps=45)
    fig.tight_layout()
    plt.show()


def loadScalaFlow():
    """
    How to load the density files in from the Scalaflow Dataset
    :return:
    """

    input_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow',
                                  'sim_000070', 'density_000150.npz')
    # input_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow',
    #                               'sim_000070', 'velocity_000150.npz')

    data = np.load(input_filepath)
    lst = data.files
    array = data[lst[0]]
    print(np.shape(array))
    array = array.T  # transpose cause its shape is messed up
    array = array[0]  # remove the upper array filled with the actually 3D array
    array = np.flip(array)  # array is upside down
    print(np.amin(array), np.amax(array))

    plot_3darray(array)


def loadYTIsolatedGalaxy():
    """
    https://yt-project.org/doc/quickstart/index.html
    :return:
    """

    input_filepath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'IsolatedGalaxy',
                                  'galaxy0030', 'galaxy0030')
    ds = yt.load(input_filepath)

    # https://yt-project.org/doc/examining/low_level_inspection.html?highlight=covering_grid
    # all_data_level_0 = ds.covering_grid(level=0, left_edge=[0, 0.0, 0.0], dims=ds.domain_dimensions)
    all_data_level_2 = ds.covering_grid(level=2, left_edge=[0, 0.0, 0.0], dims=ds.domain_dimensions * 2 ** 2)
    #
    plot_3darray(all_data_level_2["gas", "density"], logarithmicColor=True)

    # https://yt-project.org/doc/visualizing/manual_plotting.html
    # slc = ds.slice("z", 0.5)
    # frb = slc.to_frb((20, "kpc"), 512)
    # frb.apply_gauss_beam(nbeam=30, sigma=2.0)
    # frb.apply_white_noise(5e-23)
    # plt.imshow(frb["gas", "density"].d)
    # plt.show()

    # https://yt-project.org/doc/quickstart/data_objects_and_time_series.html
    # https://yt-project.org/doc/examining/low_level_inspection.html?highlight=covering_grid
    # https://yt-project.org/doc/cookbook/calculating_information.html?highlight=covering_grid
    # https://yt-project.org/doc/quickstart/data_objects_and_time_series.html?highlight=covering_grid
    # https://yt-project.org/doc/visualizing/plots.html?highlight=covering_grid
    # https://yt-project.org/doc/visualizing/volume_rendering.html


def main():
    print('moin moin')

    # loadYTIsolatedGalaxy()
    loadScalaFlow()


if __name__ == "__main__":
    # execute only if run as a script
    main()