import numpy as np
import vtk
import os
from vtk.util.numpy_support import vtk_to_numpy
import png
import math
import time
import multiprocessing as mp

def list_files_with_ext(directory, ext):
    '''
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    '''
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist

def trim_empty_array(np_array, vmin=None, vmax=None):
    '''
    DELETES ALL EMPTY FRAMES meaning only 0, BE CAREFUL
    delete the sub lists that have the same min and max value
    :param np_array:
    :param vmin: optional minimum cut of values
    :param vmax: optional maximum cut of values
    :return: 3d np array
    '''

    if vmin is None:
        vmin = 0
    if vmax is None:
        vmax = 0


    delete_list = []
    for i in range(len(np_array)):
        max_value = np.amax(np_array[i])
        min_value = np.amin(np_array[i])
        if max_value < vmin or min_value > vmax:
            delete_list.append(i)
    return np.delete(np_array, delete_list, axis=0), delete_list

def cut_cells(np_array, square):
    '''
    elects a rectangle of cells from a 2d array
    :param np_array: data array
    :param square: (x, y, width, height)
        x coordinate of the square, left corner
        y coordinate of the square, upper corner
        width of the square
        height: hight of the square
    :return: returns the trimmed array
    '''
    # selects a rectangle of cells only ints allowed
    # x, y The top and left rectangle coordinates
    # width, height are the rectangle width and height

    (x, y, width, height) = square

    if width > np_array.shape[1] or height > np_array.shape[2]:
        raise RuntimeError('width or height cant be taller than the data')

    new_np_array = []
    for array in np_array:
        new_np_array.append(array[y: (y + height), x:(x + width)])

    return np.array(new_np_array)

def vtk_to_array(filename):
    '''
    loads vtk file as a np array
    :param filename: director of the vtk file
    :return: 3d np array
    '''
    # for importing vtk files from paraview and refolding it into a 3d texture

    reader = vtk.vtkStructuredPointsReader()
    reader.SetFileName(filename)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data = reader.GetOutput()
    dim = data.GetDimensions()

    # shape of data
    # print(dim)

    numpy_array = vtk_to_numpy(data.GetPointData().GetArray('qc'))
    numpy_array = numpy_array.reshape(dim[2], dim[1], dim[0])

    return numpy_array

def save_PNG(data, dir):
    '''
    saves data at dir
    :param data: 2d array of int values between 0 and 2^16
    :param dir: directory of the png file
    :return:
    '''
    with open(dir, 'wb') as f:
        writer = png.Writer(width=data.shape[1], height=data.shape[0], bitdepth=16, greyscale=True)
        writer.write(f, data)

    # does the same ting as above
    # png.from_array(data, mode="L;16").save(dir)

def rescale_values_16bit(data, vmin=None, vmax=None):
    '''
    takes any np array. and rescales it to use the full range of all 16bit integer values.
    :param data: data array of numbers
    :param vmin: minimum value to be set the new 0, values that were smaller will also be 0
    :param vmax: maximum value, values that are larger will be capt at the maximum
    :return: returns np array with 16bit int of the data shape
    '''

    # minus 1 because we start from 0
    bit_range = (2 ** 16)-1

    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the int16 limit or under
        data = data.clip(min=vmin, max=vmax)

    rescaled_data = (bit_range * ((data - vmin) / value_range))
    rescaled_data = rescaled_data.astype(np.uint16)

    return rescaled_data

def tile_array(threed_array, makesqr=True):
    '''
    takes 3d array and tiles all the slices of the z dimension next to each other in a 2d array
    :param threed_array: 3d data array of the shape (z, x, y)
    :param makesqr: if true the output array will have a shape of a multiple of 2
    :return: 2d np array
    '''
    #

    (z_length, x_length, y_length) = threed_array.shape

    # print(col_and_row_number, twod_x_length, twod_y_length)
    # new array

    if makesqr:
        minimum_new_im_range_required = max(x_length, y_length)
        twod_array_length = 2
        while minimum_new_im_range_required > twod_array_length:
            twod_array_length = twod_array_length * 2

        new_x_length, new_y_length = twod_array_length, twod_array_length
        x_offset, y_offset = 0, 0
        z=0
        while z < z_length:
            # next column
            x_offset += x_length
            # new row
            if (x_offset > (new_x_length - x_length)):
                if new_x_length == new_y_length:
                    # expand to the side
                    new_x_length += new_x_length
                else:
                    # expand below
                    new_y_length += new_y_length
                    x_offset = 0
                    y_offset += y_length

            z = math.ceil((new_x_length/x_length)*(new_y_length/y_length))
            # print(new_x_length, new_y_length, x_offset, y_offset, z)

        twod_array = np.zeros((new_y_length, new_x_length), dtype=np.uint16)
    else:
        col_and_row_number = math.ceil(math.sqrt(z_length))  # number rows and columns needed to make a square tiling
        twod_x_length, twod_y_length = col_and_row_number * x_length, col_and_row_number * y_length  # min required size of
        # if we dont care about the sqrt resolution well just use the minimum required
        twod_array = np.zeros((twod_x_length, twod_y_length), dtype=np.uint16)

    # place the slices of the 3d array in the new empty 2d array
    x_offset, y_offset = 0, 0
    (new_x_length, new_y_length) = twod_array.shape
    for z in range(z_length):
        # place array (x and y are flipped cause im dumb, but has to stay like this)
        twod_array[y_offset:y_offset + y_length, x_offset:x_offset + x_length] = threed_array[z]

        # next column
        x_offset += x_length

        # new row
        if (x_offset > (new_y_length - x_length)):
            x_offset = 0
            y_offset += y_length

        # print(new_x_length, new_y_length, x_offset, y_offset)

    if makesqr:
        number_of_squares = math.ceil((new_x_length/x_length)*(new_y_length/y_length))
    else:
        number_of_squares = col_and_row_number*col_and_row_number

    # squared col_and_row_number because the other tiles will be empty but needed for proper scaling
    shape = (number_of_squares, x_length, y_length)

    return twod_array, shape
    
def convert_vtk_to_png(vtk_path, png_path=None, vmin=None, vmax=None, square=None, delete_list=[]):
    '''
    opens vtk file and save it as png
    guide:https://stackoverflow.com/questions/25696615/can-i-save-a-numpy-array-as-a-16-bit-image-using-normal-enthought-python/25814423
    :param vtk_path: dir of the vtk
    :param png_path: optional dir of the png
    :param vmin: optional minimum value of the png, everything lower will be black
    :param vmax: optional maximum vale of the png, everything higher will be white
    :param square: (x, y, width, height)
        x coordinate of the square, left corner
        y coordinate of the square, upper corner
        width of the square
        height: hight of the square
    ;param delete_list: Array if z indexes to delete in the vtk array
    :return:
    '''
    vtk_array = vtk_to_array(vtk_path)
    # print(np.shape(vtk_array))

    # print('maximum value: ', np.amax(vtk_array), ' and minimum value: ', np.amin(vtk_array))

    # check if square are being used, else dont cut array
    if not (square is None):
        vtk_array = cut_cells(vtk_array, square=square)

    # trim z dimension, that should only contain empty
    vtk_array = np.delete(vtk_array, delete_list, axis=0)

    # rescale array to be 16bit int range
    vtk_array = rescale_values_16bit(vtk_array, vmin, vmax)

    vtk_array, shape = tile_array(vtk_array)

    # check if png is being used, else save png with next to original
    if (png_path == '') or (png_path is None):
        png_path = os.path.splitext(vtk_path)[0] + '.png'
    # insert dimensions into path
    png_path = os.path.splitext(png_path)[0]\
               + '_x' + str(shape[1])\
               + '_y' + str(shape[2])\
               + '_z' + str(shape[0])\
               + os.path.splitext(png_path)[1]

    save_PNG(vtk_array, png_path)
    # print('saved to ', png_path)

def folder_vtk_to_png(folder_path, output_path=None, vmin=None, vmax=None, square=None, delete_list=[]):
    '''
    translate all vtk files in folder to pngs
    :param folder_path: dir where the vtk files are
    :param output_path: optional outputpath for the pngs, otherwise they will be in the folder_path dir
    :param vmin: optional minimum value of the png, everything lower will be black
    :param vmax: optional maximum vale of the png, everything higher will be white
    :param square: (x, y, width, height)
        x coordinate of the square, left corner
        y coordinate of the square, upper corner
        width of the square
        height: hight of the square
    :return:
    '''

    if not os.path.exists(output_path):
        os.mkdir(output_path)

    vtk_files = list_files_with_ext(folder_path, '.vtk')

    to_do_list = []

    # number_of_files = len(vtk_files)
    # current_index = 0
    # start_time = time.time()

    for vtk_file in vtk_files:
        # # printout of the time and percentage done
        # current_index+= 1
        # current_precent = str(round((current_index/number_of_files)*100, 1))
        # elapsed_time = time.time() - start_time
        # start_time = time.time()
        # estimated_time = (number_of_files-current_index)*elapsed_time
        # estimated_time_str = time.strftime('%H:%M:%S', time.gmtime(estimated_time))
        # print(current_precent, '%, ETC:', estimated_time_str)

        # check if output_path is used, if so create the current output file name
        if not ((output_path == '') or (output_path is None)):
            vtk_filename = os.path.splitext(os.path.basename(vtk_file))[0]
            output_filepath = os.path.join(output_path, vtk_filename + '.png')
        else:
            output_filepath = None

        # builds a list that has all the arguments for each process
        to_do_list.append((vtk_file, output_filepath, vmin, vmax, square, delete_list))

    # runs mutiprossessing for every file in the list of files
    with mp.Pool(processes=4) as pool:
        pool.starmap(convert_vtk_to_png, to_do_list)

    print('finished converting ', folder_path)

def vtk_range(vtk_path, vmin=None, vmax=None):
    '''
    returns the value range of a vtk file
    :param vtk_path: file path of the vtk file
    :return: maximum and minimum value in the vtk
    '''
    vtk_array = vtk_to_array(vtk_path)
    # maximum = np.amax(vtk_array)
    vtk_array, delete_array  = trim_empty_array(vtk_array, vmin=vmin, vmax=vmax)
    (z, x, y) = np.shape(vtk_array)

    # return maximum, z
    return z, delete_array

def folder_vtk_range(folder_path, vmin=None, vmax=None):
    '''
    prints the average mean and min and max of the maxiumum values in a folder of vtk files
    :param folder_path: path of the vtk foles you want to analyse
    :return:
    '''
    vtk_files = list_files_with_ext(folder_path, '.vtk')

    maximum_list = []
    z_list = []
    delete_list_list = []
    number_of_files = len(vtk_files)
    current_index = 0
    for vtk_file in vtk_files:
        current_index+= 1
        print(str(round((current_index/number_of_files)*100, 1)), '%')

        # maximum, z = vtk_range(vtk_file, vmin=vmin, vmax=vmax)
        z, delete_list = vtk_range(vtk_file, vmin=vmin, vmax=vmax)
        # maximum_list.append(maximum)
        z_list.append(z)
        delete_list_list.append(delete_list)

    # print('Of the Maxima:')
    # print('maximum value: ', np.amax(maximum_list), ' and minimum value: ', np.amin(maximum_list))
    # print('mean: ', np.mean(maximum_list))
    # print('median: ', np.median(maximum_list))

    print('Of the Z with vmin=', vmin, ' and vmax=', vmax)
    print('maximum value: ', np.amax(z_list), ' and minimum value: ', np.amin(z_list))
    print(z_list)
    print(delete_list_list)

    #minima doesnt make much sense since its always 0

def displace(threed_array, displacement_array):
    '''
    use displacement map to displace array entries, but results are disappointing
    :param threed_array: 3d array to be displaced
    :param displacement_array: 2d array on how much the array should be displaced
    :return:
    '''

    (z_length, x_length, y_length) = threed_array.shape
    z_extended = 64 # should be calculated but dont need it for test

    threed_array_ext = np.zeros((z_extended-z_length, x_length, y_length))
    new_threed_array = np.concatenate((threed_array, threed_array_ext), axis=0)

    for x in range(x_length):
        for y in range(y_length):
            displacement = displacement_array[x][y]
            for z in range(z_length):
                new_location = z - displacement
                if 0 < new_location < z_extended:
                    new_threed_array[new_location][x][y] = threed_array[z][x][y]

    return new_threed_array


def save_curve(file_path, var_selection, output_filepath=None, ):
    """

    :param file_path:
    :param var_selection:
    :param output_filepath:
    :return:
    """

    # read nc file
    datastructure = nc.Dataset(file_path)

    # get data into dict
    data = {}
    for var in var_selection:
        (lowerbound, fullrange) = var_selection[var]
        data[var] = normalize_data(datastructure[var][lowerbound: (lowerbound + fullrange)])

    string_shape = ''
    for var in var_selection:  # printing out the range and offset
        minimum_value = datastructure[var][var_selection[var][0]]
        value_range = datastructure[var][var_selection[var][0] + var_selection[var][1] - 1] - minimum_value
        string_shape += '_' + var + '_m' + str(minimum_value) + '_r' + str(value_range)

    if output_filepath is None:  # if None is given it'll be saved next to file as txt
        csv_path = os.path.splitext(file_path)[0] + string_shape + '.csv'
    else:  # if logpath is given extension is gut and set to txt just to be save
        csv_path = os.path.splitext(output_filepath)[0] + string_shape + '.csv'

    # build df columns
    columns = ['time']
    for var in var_selection:
        columns.append(var)
    df = pd.DataFrame(columns=columns)

    # get the smallest range so all rows a filled
    smallest = min([len(data[var]) for var in data])
    for i in range(smallest):  # for the range of the smallest of the vars we read the index at the proportional index
        percentage = i / (smallest - 1)  # range from 0 to 1
        new_row = {'time': percentage}
        for var in var_selection:
            index = int(percentage * (len(data[var]) - 1))
            new_row[var] = data[var][index]

        df_length = len(df)
        df.loc[df_length] = new_row  # append new row (not using 'append' cause that copies the df)

    df.to_csv(csv_path, header=True, index=False)  # save data fame as csv


def main():
    vtk_filename = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view',
                                'cm1out', 'cm1out_001030.vtk')
    vtk_foldername = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                  'rawdata_view', 'cm1out_qc')

    # original shape of vtk files is (40, 1140, 1140)
    # square = (267, 257, 585, 585) # selection to fit 7x7 4k texture
    # square = (58, 58, 1024, 1024)  # selection to have all data and 8k res
    square = (314, 314, 512, 512) # selection to have all data and 4k res

    # vmin, vmax, delete_list = 0.0001, 0.0005, [0, 33, 34, 35, 36, 37, 38, 39] # range where only 32 or lower z lines remain
    vmin, vmax, delete_list = 0.0001, 0.0028, [0, 33, 34, 35, 36, 37, 38, 39]
    # output_filename = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
    #                                  'rawdata_view', 'cm1out_qc_vmax' + str(vmax), 'cm1out_001030_qc.png')
    output_filename = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                     'rawdata_view', 'cm1out_001030_qc_vmin' + str(vmin) + '_vmax' + str(vmax) + '.png')
    output_foldername = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                     'rawdata_view', 'cm1out_qc_vmin' + str(vmin) + '_vmax' + str(vmax) + '_3k')

    # displacement_file = 'sphere_displacement_6365.npy'
    # with open(displacement_file, 'rb') as f:
    #     displacement_array = np.load(f)

    # read vtk file and transform it to png
    convert_vtk_to_png(vtk_path=vtk_filename, png_path=output_filename, vmin=vmin, vmax=vmax, square=square,
                       delete_list=delete_list)

    # read vtk folder and transfrom all vtk files to png
    # folder_vtk_to_png(vtk_foldername, output_path=output_foldername, vmin=vmin, vmax=vmax, square=square,
    #                   delete_list=delete_list)

    # prints range of vtk files in folder
    # folder_vtk_range(vtk_foldername, vmin=vmin, vmax=vmax)


if __name__ == "__main__":
    # execute only if run as a script
    main()