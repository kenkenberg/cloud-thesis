import numpy as np
import vtk
import os
from vtk.util.numpy_support import vtk_to_numpy
from PIL import Image
import math
import time
import multiprocessing as mp

def vtk_to_array(filename, array=['uinterp', 'vinterp', 'winterp']):
    '''
    loads vtk file as a np array
    :param filename: director of the vtk file
    :return: 3d np array
    '''
    # for importing vtk files from paraview and refolding it into a 3d texture

    reader = vtk.vtkStructuredPointsReader()
    reader.SetFileName(filename)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data = reader.GetOutput()
    dim = data.GetDimensions()

    # shape of data
    # print(dim)

    collection =[]
    for item in array:
        numpy_array = vtk_to_numpy(data.GetPointData().GetArray(item))
        collection.append(numpy_array.reshape(dim[2], dim[1], dim[0]))

    return collection

def cut_cells(np_array, square):

    # selects a rectangle of cells only ints allowed
    # x, y The top and left rectangle coordinates
    # width, height are the rectangle width and height

    (x, y, width, height) = square

    new_np_array = []
    for sub_array in np_array:
        new_subarray=[]
        for line in sub_array:
            new_subarray.append(line[y: (y + height), x:(x + width)])
        new_np_array.append(np.array(new_subarray))

    return np.array(new_np_array)

def rescale_values_8bit(data, vmin=None, vmax=None):
    '''
    takes any np array. and rescales it to use the full range of all 16bit integer values.
    :param data: data array of numbers
    :param vmin: minimum value to be set the new 0, values that were smaller will also be 0
    :param vmax: maximum value, values that are larger will be capt at the maximum
    :return: returns np array with 16bit int of the data shape
    '''

    # minus 1 because we start from 0
    bit_range = (2 ** 8)-1

    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the int16 limit or under
        data = data.clip(min=vmin, max=vmax)

    rescaled_data = (bit_range * ((data - vmin) / value_range))
    rescaled_data = rescaled_data.astype(np.uint8)

    return rescaled_data


def tile_array(threed_array, makesqr=True):

    (z_length, x_length, y_length) = threed_array.shape

    # print(col_and_row_number, twod_x_length, twod_y_length)
    # new array

    if makesqr:
        minimum_new_im_range_required = max(x_length, y_length)
        twod_array_length = 2
        while minimum_new_im_range_required > twod_array_length:
            twod_array_length = twod_array_length * 2

        new_x_length, new_y_length = twod_array_length, twod_array_length
        x_offset, y_offset = 0, 0
        z=0
        while z < z_length:
            # next column
            x_offset += x_length
            # new row
            if (x_offset > (new_x_length - x_length)):
                if new_x_length == new_y_length:
                    # expand to the side
                    new_x_length += new_x_length
                else:
                    # expand below
                    new_y_length += new_y_length
                    x_offset = 0
                    y_offset += y_length

            z = math.ceil((new_x_length/x_length)*(new_y_length/y_length))
            # print(new_x_length, new_y_length, x_offset, y_offset, z)

        twod_array = np.zeros((new_y_length, new_x_length), dtype=np.uint16)
    else:
        col_and_row_number = math.ceil(math.sqrt(z_length))  # number rows and columns needed to make a square tiling
        twod_x_length, twod_y_length = col_and_row_number * x_length, col_and_row_number * y_length  # min required size of
        # if we dont care about the sqrt resolution well just use the minimum required
        twod_array = np.zeros((twod_x_length, twod_y_length), dtype=np.uint16)

    # place the slices of the 3d array in the new empty 2d array
    x_offset, y_offset = 0, 0
    (new_x_length, new_y_length) = twod_array.shape
    for z in range(z_length):
        # place array (x and y are flipped cause im dumb, but has to stay like this)
        twod_array[y_offset:y_offset + y_length, x_offset:x_offset + x_length] = threed_array[z]

        # next column
        x_offset += x_length

        # new row
        if (x_offset > (new_y_length - x_length)):
            x_offset = 0
            y_offset += y_length

        # print(new_x_length, new_y_length, x_offset, y_offset)

    if makesqr:
        number_of_squares = math.ceil((new_x_length/x_length)*(new_y_length/y_length))
    else:
        number_of_squares = col_and_row_number*col_and_row_number

    # squared col_and_row_number because the other tiles will be empty but needed for proper scaling
    shape = (number_of_squares, x_length, y_length)

    return twod_array, shape


def save_PNG(data, dir):
    # with open(dir, 'wb') as f:
    #     writer = png.Writer(width=data.shape[1], height=data.shape[0], greyscale=False)
    #     writer.write(f, data)

    new_im = Image.fromarray(data)
    new_im.save(dir)
    # does the same ting as above
    # png.from_array(data, mode="L;16").save(dir)


def convert_vtk_to_png(vtk_path, png_path=None, vmin=None, vmax=None, square=None, delete_list=[]):
    vtk_array = vtk_to_array(vtk_path)

    # print('maximum value: ', np.amax(vtk_array), ' and minimum value: ', np.amin(vtk_array))

    # check if square are being used, else dont cut array
    if not (square is None):
        vtk_array = cut_cells(vtk_array, square=square)

    # trim z dimension, that should only contain empty
    new_array = []
    for sub_array in vtk_array:
        new_array.append(np.delete(sub_array, delete_list, axis=0))
    vtk_array = np.array(new_array)

    # multiply with -1 to invert every single value because x,y,z is reverse in unreal
    vtk_array = vtk_array * -1
    vmin = vmin * -1
    vmax = vmax * -1

    # rescale array to be 8bit int range
    vtk_array = rescale_values_8bit(vtk_array, vmin, vmax)

    shape = 0
    new_array = []
    for sub_array in vtk_array:
        new_subarray, shape = tile_array(sub_array)
        new_array.append(new_subarray)
    vtk_array = new_array

    # # make tuple
    r, g, b = vtk_array[0], vtk_array[1], vtk_array[2]
    vtk_array = np.dstack((r,g,b)).astype(np.uint8)

    # check if png is being used, else save png with next to original
    if (png_path == '') or (png_path is None):
        png_path = os.path.splitext(vtk_path)[0] + '.png'
    # insert dimensions into path
    png_path = os.path.splitext(png_path)[0]\
               + '_x' + str(shape[1])\
               + '_y' + str(shape[2])\
               + '_z' + str(shape[0])\
               + os.path.splitext(png_path)[1]

    # print(np.shape(vtk_array))
    save_PNG(vtk_array, png_path)
    # # print('saved to ', png_path)

def main():

    vtk_filename = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view',
                                'cm1out_001030.vtk')

    # original shape of vtk files is (40, 1140, 1140)
    # square = (267, 257, 585, 585) # selection to fit 7x7 4k texture
    square = (58, 58, 1024, 1024)  # selection to have all data and 8k res
    # square = (314, 314, 512, 512) # selection to have all data and 4k res

    # vmin, vmax, delete_list = 0.0001, 0.0005, [0, 33, 34, 35, 36, 37, 38, 39] # range where only 32 or lower z lines remain
    vmin, vmax, delete_list = -50, 50, [0, 33, 34, 35, 36, 37, 38, 39]
    output_filename = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets',
                                     'rawdata_view',  'cm1out_001030_uinterp_vinterp_winterp.png')

    # read vtk file and transform it to png
    convert_vtk_to_png(vtk_path=vtk_filename, png_path=output_filename, vmin=vmin, vmax=vmax, square=square,
                       delete_list=delete_list)

if __name__ == "__main__":
    # execute only if run as a script
    main()