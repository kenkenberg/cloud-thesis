import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import pyrr
import pickle as pkl
import os

def plot(numpy_array, filename='', vmin=None, vmax=None, debug=False):
    Z = numpy_array[int(len(numpy_array)/2)]
    (frames, xres, yres) = np.shape(numpy_array)
    fig = plt.figure(figsize=(xres/100, yres/100), dpi=100)
    im = plt.imshow(Z, interpolation='none', cmap='gray', vmin=vmin, vmax=vmax)
    if debug:
        text = plt.text(50, 50, '---', color='red', size=24)

    def animate_func(i):
        im.set_array(numpy_array[i])
        if debug:
            text.set_text(str(i).zfill(3))
            plt.savefig(filename + "/" + str(i).zfill(3) + ".png")
            # if i == frames - 1:
            #     print(f'{i + 1} == {frames} frames closing!')
            #     plt.close(fig)
        return [im]

    anim = animation.FuncAnimation(fig, animate_func, frames=frames, interval=1, repeat=True)

    plt.axis('equal')
    plt.axis('off')
    # plt.tight_layout()
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

    plt.show()

def make_displacement(array_shape, sphere_radius,):
    '''
    projects an an array plane onto a sphere and returns the distance as int between the the plane and the sphere
    the sphere is centered in the middle of the plane so that the surface of the sphere touches the center
    :param array_shape: a 2D shape of the plane
    :param sphere_radius: radius of the sphere returns null if sphere not large enough
    :return: returns np array of the array_shape filled with int
    '''

    projecton_dir = np.array((-1, 0, 0))
    sphere_position = np.array((-sphere_radius, array_shape[0]/2, array_shape[1]/2))

    # array = np.zeros(array_shape, int)
    sphere = np.append(sphere_position, sphere_radius)

    array = np.zeros(array_shape, int)
    for x in range(array_shape[0]):
        for y in range(array_shape[1]):
            print(x, y)
            ray = np.array(([0, x, y], projecton_dir))
            intersection = pyrr.geometric_tests.ray_intersect_sphere(ray=ray, sphere=sphere)
            move_by = int(intersection[1][0])
            array[x][y] = move_by
    return array

def main():
    array_shape = np.array((1024, 1024))
    sphere_radius = 6360 + 5
    output_file = 'sphere_displacement.npy'

    array = make_displacement(array_shape, sphere_radius)
    with open(output_file, 'wb') as f:
        np.save(f, array)

if __name__ == "__main__":
    # execute only if run as a script
    main()