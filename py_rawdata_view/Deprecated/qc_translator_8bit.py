import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import vtk
import os
from vtk.util.numpy_support import vtk_to_numpy
import pandas as pd
from PIL import Image
import math
import shutil

def plot(numpy_array, frames, save='off', filename='', xres=1000, yres=1000, vmin=None, vmax=None, debug=False, makesqr=False):
    # plots an numpy array that has basically a gif stored in it.
    # numpy_array[0] is the first frame
    # numpy_array[0][0] should contain the first row
    # numpy_array[0][0][0] should contain the first pixel top left
    # the value doesnt matter it scales the colors automatically
    # xres, yres are the pixel resolution of the image ( if you save the image dont rescale the window with the mouse
    # vmin, vmax are the minimum and maximum values the gray scale will take
    # debug=true will turn the frame counter in the top left
    # save has the possible states
    #   'off' that will not save the image,
    #   'gif' which will save a gif,
    #   'png' which will save as a volume texture
    # makesqr=true only works with save as png and will force the output image to be a resolution of a multiple of 2

    Z = numpy_array[int(len(numpy_array)/2)]
    fig = plt.figure(figsize=(xres/100, yres/100), dpi=100)
    im = plt.imshow(Z, interpolation='none', cmap='gray', vmin=vmin, vmax=vmax)
    if debug:
        text = plt.text(50, 50, '---', color='white', size=24)
    def animate_func(i):
        im.set_array(numpy_array[i])
        if debug:
            text.set_text(str(i).zfill(3))
        if save == 'png':
            plt.savefig(filename + "/" + str(i).zfill(3) + ".png")
            if i == frames - 1:
                print(f'{i + 1} == {frames} frames closing!')
                plt.close(fig)
        return [im]

    if save == 'png':
        if not os.path.exists(filename):
            os.mkdir(filename)
        anim = animation.FuncAnimation(fig, animate_func, frames=frames, interval=100, repeat=False)
    else:
        anim = animation.FuncAnimation(fig, animate_func, frames=frames, interval=100, repeat=True)
    plt.axis('equal')
    plt.axis('off')
    # plt.tight_layout()
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
    if save=='gif':
        anim.save(filename + '.gif', writer='imagemagick', fps=10)
        print('saved file ', filename)
    plt.show()

    if save == 'png':
        concat_pngs(filename, makesqr=makesqr)

def vtk_to_array(filename):
    # for importing vtk files from paraview and refolding it into a 3d texture

    reader = vtk.vtkStructuredPointsReader()
    reader.SetFileName(filename)
    reader.ReadAllVectorsOn()
    reader.ReadAllScalarsOn()
    reader.Update()

    data = reader.GetOutput()
    dim = data.GetDimensions()
    print(dim)

    numpy_array = vtk_to_numpy(data.GetPointData().GetArray('qc'))
    numpy_array = numpy_array.reshape(dim[2], dim[1], dim[0])

    return numpy_array

def csv_to_array(filename, x, y, z):
    # for importing one dimentional csv files from paraview.
    # x y z are the dimentions the 3d texture had before paraview flattened it.
    # please take note that csv reading is like 10 times slower than vtk reading so be patient

    # if you cant import csv this will also do just fine but it feels even slower than the csv reader
    numpy_array = np.genfromtxt(filename, skip_header=True)
    numpy_array = numpy_array.reshape(z, y, x)
    return numpy_array

def save_to_csv(numpy_array, filename):
    # make them a flat list again
    numpy_array = numpy_array.flatten(order='K')
    # save as csv
    df = pd.DataFrame({'qc': numpy_array.tolist()})
    df.to_csv(filename, index=False)

def concat_pngs(dir, makesqr=False):
    # adds all files in directory to a large file. it will line them up to from a square
    # makesqr=True makes the resolution to a multiple of 2 cause engine like it that way

    filelist = list_files_with_ext(dir, 'png')
    images = [Image.open(x) for x in filelist]

    widths, heights = zip(*(i.size for i in images))
    max_width = max(widths)
    max_height = max(heights)
    image_number = images.__len__()
    col_and_row_number = math.ceil(math.sqrt(image_number))

    if makesqr:
        minimum_new_im_range = max(max_width, max_height)*col_and_row_number
        new_im_range = 2
        while minimum_new_im_range > new_im_range:
            new_im_range=new_im_range*2
        new_im = Image.new('RGB', (new_im_range, new_im_range))
    else:
        new_im = Image.new('RGB', (max_width*col_and_row_number, max_height*col_and_row_number))

    x_offset, y_offset = 0, 0
    for im in images:
        new_im.paste(im, (x_offset, y_offset))
        x_offset += max_width
        if(x_offset>(new_im.size[0]-max_width)):
            x_offset = 0
            y_offset += max_height

    # for im in images:
    #     im.close()

    parentdir = os.path.dirname(filelist[0])
    new_filename = parentdir + '_' + str(max_width) + 'X_' + str(max_height) + 'Y_' + str(image_number) + 'Z' + '.png'
    new_im.save(new_filename)
    print('saved file ', new_filename)

    #delete temporary files
    if os.path.exists(dir):
        shutil.rmtree(dir)

def list_files_with_ext(dir, ext):
    filelist = []
    for file in os.listdir(dir):
        if file.endswith(ext):
            filelist.append(os.path.join(dir, file))
    return filelist

def trim_empty_array(np_array, vmin=None, vmax=None):
    # DELETES ALL EMPTY FRAMES, BE CAREFUL
    # delete the sub lists that have the same min and max value
    if vmin == None:
        vmin = 0
    if vmax == None:
        vmax = 0

    delete_list = []
    for i in range(len(np_array)):
        max_value = np.amax(np_array[i])
        min_value = np.amin(np_array[i])
        if max_value < vmin or min_value > vmax:
            delete_list.append(i)
    return np.delete(np_array, delete_list, axis=0)

def cut_cells(np_array, x, y, width, height):
    # selects a rectangle of cells only ints allowed
    # x, y The top and left rectangle coordinates
    # width, height are the rectangle width and height
    new_np_array = []
    for array in np_array:
        new_np_array.append(array[y: (y + height), x:(x + width)])
    return new_np_array

vtk_filename = os.path.join('D:/', 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view', 'cm1out_qc', 'cm1out_001030_qc.vtk')
# vtk_filename = os.path.join('D:/', 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view', 'cm1out_000800_qc.vtk')
# vtk_filename = os.path.join('D:/', 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view', 'cm1out_000600_qc.vtk')
# vtk_filename = os.path.join('D:/', 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view', 'cm1out_000146_qc.vtk')
filename= os.path.splitext(vtk_filename)[0]
numpy_array = vtk_to_array(vtk_filename)

# csv_filename = os.path.join('D:/', 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view', 'cm1out_001030_qc_rescaled.csv')
# numpy_array = csv_to_array(csv_filename, xres, yres, zres)

print('maximum value: ', np.amax(numpy_array))
print('minimum value: ', np.amin(numpy_array))
vmin, vmax = np.amin(numpy_array), np.amax(numpy_array) # actual range
# vmin, vmax = 0.0005, 0.0006 # high contrast mode form daniel
# vmin, vmax = 0.0003, 0.0006 # more whispy cloud look

# DELETES ALL EMPTY FRAMES, BE CAREFUL
# numpy_array = trim_empty_array(numpy_array, vmin=vmin, vmax=vmax)
numpy_array = cut_cells(numpy_array, 195, 195, 750, 750) # selection daniel
# numpy_array = cut_cells(numpy_array, 229, 229, 682, 682) # selection to fit 6x6 4k texture
# numpy_array = cut_cells(numpy_array, 267, 257, 585, 585) # selection to fit 7x7 4k texture

xres, yres, zres = len(numpy_array[0]), len(numpy_array[0][0]), len(numpy_array)
print(xres, yres, zres)

# one just to view one saves as 3d texture
# plot(numpy_array, frames=zres, save='gif', filename=filename,
#      xres=xres, yres=yres, vmin=vmin, vmax=vmax, makesqr=True)
# plot(numpy_array, frames=zres, save='png', filename=filename,
#      xres=xres, yres=yres, vmin=vmin, vmax=vmax, makesqr=True)
plot(numpy_array, frames=zres, xres=xres, yres=yres, vmin=vmin, vmax=vmax, debug=True)

# rescale the values to be from 0 to 1
# numpy_array = np.interp(numpy_array, (numpy_array.min(), numpy_array.max()), (0, 1))
# save_to_csv(numpy_array, "cm1out_001030_qc_rescaled.csv")