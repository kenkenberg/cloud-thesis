# trace generated using paraview version 5.8.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'NetCDF Reader'
cm1out_000146nc = NetCDFReader(FileName=['D:\\BoringStuff\\Uni-LMU-MMI\\Master\\Assets\\rawdata_view\\cm1out_000146.nc'])
cm1out_000146nc.Dimensions = '(one)'

# Properties modified on cm1out_000146nc
cm1out_000146nc.Dimensions = '(nk, nj, ni)'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1608, 796]

# get layout
layout1 = GetLayout()

# show data in view
cm1out_000146ncDisplay = Show(cm1out_000146nc, renderView1, 'UniformGridRepresentation')

# trace defaults for the display properties.
cm1out_000146ncDisplay.Representation = 'Outline'
cm1out_000146ncDisplay.ColorArrayName = [None, '']
cm1out_000146ncDisplay.OSPRayScaleArray = 'prs'
cm1out_000146ncDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
cm1out_000146ncDisplay.SelectOrientationVectors = 'None'
cm1out_000146ncDisplay.ScaleFactor = 113.9
cm1out_000146ncDisplay.SelectScaleArray = 'None'
cm1out_000146ncDisplay.GlyphType = 'Arrow'
cm1out_000146ncDisplay.GlyphTableIndexArray = 'None'
cm1out_000146ncDisplay.GaussianRadius = 5.695
cm1out_000146ncDisplay.SetScaleArray = ['POINTS', 'prs']
cm1out_000146ncDisplay.ScaleTransferFunction = 'PiecewiseFunction'
cm1out_000146ncDisplay.OpacityArray = ['POINTS', 'prs']
cm1out_000146ncDisplay.OpacityTransferFunction = 'PiecewiseFunction'
cm1out_000146ncDisplay.DataAxesGrid = 'GridAxesRepresentation'
cm1out_000146ncDisplay.PolarAxes = 'PolarAxesRepresentation'
cm1out_000146ncDisplay.ScalarOpacityUnitDistance = 4.356408796929929
cm1out_000146ncDisplay.SliceFunction = 'Plane'
cm1out_000146ncDisplay.Slice = 19

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
cm1out_000146ncDisplay.ScaleTransferFunction.Points = [2804.967041015625, 0.0, 0.5, 0.0, 100738.21875, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
cm1out_000146ncDisplay.OpacityTransferFunction.Points = [2804.967041015625, 0.0, 0.5, 0.0, 100738.21875, 1.0, 0.5, 0.0]

# init the 'Plane' selected for 'SliceFunction'
cm1out_000146ncDisplay.SliceFunction.Origin = [569.5, 569.5, 19.5]

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# save data
SaveData('D:\BoringStuff\Uni-LMU-MMI\Master\Assets\rawdata_view\cm1out_000146_qc.vtk', proxy=cm1out_000146nc, ChooseArraysToWrite=1,
    PointDataArrays=['qc'])

# destroy cm1out_000146nc
Delete(cm1out_000146nc)
del cm1out_000146nc

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [569.5, 569.5, 3132.2178190016584]
renderView1.CameraFocalPoint = [569.5, 569.5, 19.5]
renderView1.CameraParallelScale = 805.6306535876103

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).