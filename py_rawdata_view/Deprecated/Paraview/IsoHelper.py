#### import the simple module from the paraview
from paraview.simple import *
import sys

if len(sys.argv) !=5:
	sys.exit()
	
inputpath = sys.argv[1]
outputpath = sys.argv[2]

start = int(sys.argv[3])
stop = int(sys.argv[4])

	
for i in range(start,stop+1):	#for-loop terminates before reaching end of range()

	inputfilename = 'cm1out_' + str(146 + i).zfill(6) + '.nc' 
	outputfilename = 'iso' + str(i).zfill(3) + '.ply'
	
	# create a new 'NetCDF Reader'
	ncFile = NetCDFReader(FileName=[inputpath + inputfilename])
	ncFile.Dimensions = '(nk, nj, ni)'
	ncFile.SphericalCoordinates = 0
	
	# create a new 'Extract Cells By Region'
	crop = ExtractCellsByRegion(Input=ncFile)
	crop.IntersectWith = 'Sphere'
	crop.Extractintersected = 1
	crop.ExtractionSide = 'inside'
	crop.IntersectWith.Center = [565, 565, 19.5]
	crop.IntersectWith.Radius = 350.0

	# create a new 'Contour'
	contour = Contour(Input=crop)
	contour.ContourBy = ['POINTS', 'qc']
	contour.Isosurfaces = [0.0005]
	contour.ComputeNormals = 1
	contour.ComputeGradients = 0
	contour.ComputeScalars = 0
	contour.OutputPointsPrecision = 'Same as input'
	contour.GenerateTriangles = 1
	contour.PointMergeMethod = 'Uniform Binning'
	
	# set scalar coloring
#	ColorBy(contour, ('POINTS', 'prs'))
#	contour.RescaleTransferFunctionToDataRange(True, False)
	prsLUT = GetColorTransferFunction('prs')
	
	# save data
	SaveData(outputpath + outputfilename, proxy=contour, FileType='Binary',
		EnableColoring=1,
		ColorArrayName=['POINTS', 'prs'],
		LookupTable=prsLUT,
		)
	#print("Subprocess IsoHelper: " + str((i + 1 - start) * 100 / float(stop-start)) + "% of total done.")