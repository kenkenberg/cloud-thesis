# trace generated using paraview version 5.8.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'NetCDF Reader'
cm1out_001030nc = NetCDFReader(FileName=['D:\\BoringStuff\\Uni-LMU-MMI\\Master\\Assets\\rawdata_view\\cm1out_001030.nc'])
cm1out_001030nc.Dimensions = '(nk, nj, ni)'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1716, 714]

# get layout
layout1 = GetLayout()

# show data in view
cm1out_001030ncDisplay = Show(cm1out_001030nc, renderView1, 'UniformGridRepresentation')

# trace defaults for the display properties.
cm1out_001030ncDisplay.Representation = 'Outline'
cm1out_001030ncDisplay.ColorArrayName = [None, '']
cm1out_001030ncDisplay.OSPRayScaleArray = 'prs'
cm1out_001030ncDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
cm1out_001030ncDisplay.SelectOrientationVectors = 'None'
cm1out_001030ncDisplay.ScaleFactor = 113.9
cm1out_001030ncDisplay.SelectScaleArray = 'None'
cm1out_001030ncDisplay.GlyphType = 'Arrow'
cm1out_001030ncDisplay.GlyphTableIndexArray = 'None'
cm1out_001030ncDisplay.GaussianRadius = 5.695
cm1out_001030ncDisplay.SetScaleArray = ['POINTS', 'prs']
cm1out_001030ncDisplay.ScaleTransferFunction = 'PiecewiseFunction'
cm1out_001030ncDisplay.OpacityArray = ['POINTS', 'prs']
cm1out_001030ncDisplay.OpacityTransferFunction = 'PiecewiseFunction'
cm1out_001030ncDisplay.DataAxesGrid = 'GridAxesRepresentation'
cm1out_001030ncDisplay.PolarAxes = 'PolarAxesRepresentation'
cm1out_001030ncDisplay.ScalarOpacityUnitDistance = 4.356408796929929
cm1out_001030ncDisplay.SliceFunction = 'Plane'
cm1out_001030ncDisplay.Slice = 19

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
cm1out_001030ncDisplay.ScaleTransferFunction.Points = [2805.082275390625, 0.0, 0.5, 0.0, 100747.2421875, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
cm1out_001030ncDisplay.OpacityTransferFunction.Points = [2805.082275390625, 0.0, 0.5, 0.0, 100747.2421875, 1.0, 0.5, 0.0]

# init the 'Plane' selected for 'SliceFunction'
cm1out_001030ncDisplay.SliceFunction.Origin = [569.5, 569.5, 19.5]

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(cm1out_001030ncDisplay, ('POINTS', 'qc'))

# rescale color and/or opacity maps used to include current data range
cm1out_001030ncDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
cm1out_001030ncDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'qc'
qcLUT = GetColorTransferFunction('qc')

# get opacity transfer function/opacity map for 'qc'
qcPWF = GetOpacityTransferFunction('qc')

# change representation type
cm1out_001030ncDisplay.SetRepresentationType('Volume')

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [569.5, 569.5, 3132.2178190016584]
renderView1.CameraFocalPoint = [569.5, 569.5, 19.5]
renderView1.CameraParallelScale = 805.6306535876103

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).