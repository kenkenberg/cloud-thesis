# made for ParaView-5.8.1-Windows-Python3.7-msvc2015-64bit

import os
from paraview.simple import *

# input path of the folder containing the .nc files (should work with subfolders too)
inputpath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view')
infiletype = '.nc'

# outputpath must exist or he cant save it
outpath = os.path.join(os.sep, "D:" + os.sep, 'BoringStuff', 'Uni-LMU-MMI', 'Master', 'Assets', 'rawdata_view_vtk')
outfiletype = '.vtk'
# array you want to have from the source files
outPointDataArrays=['qc']

# checking if i might have spelled one of the dirs wrong
if not os.path.exists(inputpath) or not os.path.exists(outpath):
    print('input or output path doesnt exist')
else:
    print('scanning ', inputpath)

    # for every file in this dir
    for root, dirs, files in os.walk(inputpath):
        for file in files:

            # check if the extension is the correct one
            current_filename, ext = os.path.splitext(file)
            if ext == infiletype:

                # generateing input and output file dir + filename
                current_file = os.path.join(root, file)
                output_file = os.path.join(outpath, current_filename + '_' + outPointDataArrays[0] + outfiletype)

                # check if output file already exist and skip it
                if os.path.exists(output_file):
                    print('skipped file ', current_file)
                else:
                    print('working on ', current_filename)
                    # loading  file
                    nc_file = NetCDFReader(FileName=[current_file])
                    # telling paraview dimentions, apparently needed
                    nc_file.Dimensions = '(nk, nj, ni)'
                    # saving file, filtering the dimension we are interested in
                    SaveData(output_file, proxy=nc_file,
							 ChooseArraysToWrite=len(outPointDataArrays), PointDataArrays=outPointDataArrays)
                    # clearing the file from parafiew
					Delete(nc_file)
                    del nc_file

    print('finished converting ', inputpath)