import subprocess
import time

inputpath = 'J:\\v2t\\datasets\\_kilroy_\\M10_Hurricane_Formation\\'
outputpath = 'J:\\v2t\\datasets\\lrz_kolb\\HurricanePLY\\'

numFiles = 885
steps = 20

for i in range(0,numFiles, steps):
	start = i
	stop = min(i+steps,numFiles)
	
	print(inputpath + " " + outputpath + " " + "Start: " + str(start) + " Stop:" + str(stop-1))
	subprocess.call("pvpython IsoHelper.py " + inputpath + " " + outputpath + " " + str(start) + " " + str(stop-1))
	
	print(str((i+steps)*100/float(numFiles)) +  "% of total done.")
