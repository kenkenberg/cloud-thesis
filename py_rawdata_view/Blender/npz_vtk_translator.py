import os  # read directories
import numpy as np
from pyevtk.hl import imageToVTK  # is called evtk online


def list_files_with_ext(directory, ext):
    """
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    """
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist


def npz_to_nparray(filepath):
    """
    loads npz file from location
    :param filepath: string to file location
    :return: numpy array
    """
    data = np.load(filepath)
    data = data[data.files[0]]  # normal strange operations to get the the actual array inside
    data = np.transpose(data, (3, 1, 0, 2))  # turning the inner array layer outside and rightside up
    return data[0]


def nparray_to_vtk(data, filepath, variable_name='density'):
    """
    https://github.com/pyscience-projects/pyevtk/blob/master/examples/image.py
    https://vtk.org/Wiki/VTK/Writing_VTK_files_using_python (import pyevtk instead)
    translates a 3D data array into a VTK file (the ending might change)
    :param data: simple 3D array
    :param filepath: string to location and name of the new VTK file
    :param variable_name: optionally change the name of the variable in the VTK file
    :return:
    """

    data = data.copy()  # so data flag C_CONTIGUOUS is true, I don't know why why this is suddenly so important
    imageToVTK(filepath, cellData={variable_name: data})


def npz_to_vtk(npz_filepath, vtk_filepath):
    """
    translates a npz file to vtk. specifically meant for the scalarflow dataset will probably not work for other stuff
    :param npz_filepath: filepath for the exisiting npz file
    :param vtk_filepath: filepath for the vtk file to create
    :return:
    """
    data = npz_to_nparray(npz_filepath)
    nparray_to_vtk(data, vtk_filepath)


input_folderpath = os.path.join(os.sep, 'D:' + os.sep, 'BoringStuff', 'LRZ_Daniel', 'Datasets', 'ScalarFlow',
                                'sim_000070', 'density_000150.npz')
npz_to_vtk(input_folderpath, 'density_000150')
print('done')

