# for Python 3.8 (Runs Only in LINUX because of VDB at the time of creation)
import os  # read directories
import numpy as np  # nc imports and np

# cause pycharm is being dumb and doesnt use it any other way
try:
    import pyopenvdb

    vdb = pyopenvdb
except ImportError:
    print(
        "ERROR: Python can't import pyopenvdb. Please see "
        + "VTK To OpenVDB Exporter node documentation for more information."
    )
    raise


def list_files_with_ext(directory, ext):
    """
    lists all files in dir and its sub dirs with the given extension
    :param directory: string of filepath
    :param ext: string of extension ".vtk" for example
    :return:
    """
    filelist = []
    for file in os.listdir(directory):
        if file.endswith(ext):
            filelist.append(os.path.join(directory, file))
    return filelist


def normalize(data, vmin=None, vmax=None):
    """
    Normalizes array of numbers to a range of 0-1
    :param data: array of numbers of any shape
    :param vmin: optional minimum otherwise minimum of data will be used
    :param vmax: optional maximum otherwise minimum of data will be used
    :return: normalized array
    """
    # checking if min or max are used
    if vmin == vmax:
        vmin = data.min()
        value_range = data.max() - vmin
    else:
        if vmin > vmax:
            raise RuntimeError('min can not be smaller than max')
        value_range = vmax - vmin
        # clipping the array so no values are over the limit or under
        data = data.clip(min=vmin, max=vmax)

    return (data - vmin) / value_range


def npz_to_nparray(filepath):
    """
    loads npz file from location
    :param filepath: string to file location
    :return: numpy array
    """
    data = np.load(filepath)
    data = data[data.files[0]]  # normal strange operations to get the the actual array inside

    # transpose seems to work differently on windows and linux cause this scrambles the array on linux and
    # does not turn the array right side up as it does in windows
    # datastructure[name] = np.transpose(data, (3, 1, 0, 2))  # turning the inner array layer outside and rightside up

    data = np.transpose(data, (3, 0, 1, 2))  # turning the inner array layer outside
    return data[0]


def array_to_vdb(data, filepath, variable_name='density'):
    """
    stores an 3D array into a VDB file
    :param data: 3d array
    :param filepath: string of the filepath for the new VDB file
    :param variable_name: optional name for the values otherwise it will be 'density'
    :return:
    """
    grid = vdb.FloatGrid()
    grid.copyFromArray(data)
    grid.name = variable_name
    vdb.write(filepath, grids=[grid])


def npz_tp_vdb(npz_filepath, vdb_filepath):
    """
    opens npz file, normalizes it to 0-1, stores it as vdb file
    :param npz_filepath: string for the filepath of the npz file to read
    :param vdb_filepath: string of the filepath for the new VDB file
    :return:
    """
    data = npz_to_nparray(npz_filepath)
    array_to_vdb(data, vdb_filepath)


def main():
    # vmin, vmax = 0.0, 70.0  # the same as for the png translation
    npz_filepath = "density_000150.npz"
    vdb_filepath = "density_000150.vdb"
    npz_tp_vdb(npz_filepath, vdb_filepath)
    # npz_tp_vdb(npz_filepath, vdb_filepath)
    print('done')


if __name__ == "__main__":
    # execute only if run as a script
    main()